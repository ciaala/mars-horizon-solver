import SolverSolutionTransformer from '../../src/model/SolverSolutionTransformer';
import XxlSolver from '../../src/solver/xxl-solver';

const data = {
  "productionRules": [
    {
      "id": 0,
      "input": [
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        4,
        0
      ],
      "output": [
        0,
        5,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 1,
      "input": [
        0,
        0,
        1,
        2,
        0,
        0,
        0,
        0,
        0
      ],
      "output": [
        0,
        6,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 2,
      "input": [
        0,
        0,
        4,
        0,
        0,
        0,
        0,
        0,
        0
      ],
      "output": [
        0,
        6,
        0,
        2,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 3,
      "input": [
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ],
      "output": [
        0,
        0,
        0,
        5,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 4,
      "input": [
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        0,
        0
      ],
      "output": [
        0,
        0,
        0,
        6,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 5,
      "input": [
        0,
        3,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ],
      "output": [
        0,
        0,
        4,
        4,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 6,
      "input": [
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ],
      "output": [
        0,
        0,
        5,
        0,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 7,
      "input": [
        0,
        3,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ],
      "output": [
        0,
        0,
        6,
        2,
        0,
        0,
        0,
        2,
        0
      ]
    },
    {
      "id": 8,
      "input": [
        2,
        0,
        0,
        2,
        0,
        0,
        0,
        0,
        0
      ],
      "output": [
        0,
        0,
        10,
        0,
        0,
        0,
        0,
        2,
        0
      ]
    },
    {
      "id": 9,
      "input": [
        0,
        0,
        1,
        0,
        1,
        0,
        0,
        0,
        0
      ],
      "output": [
        0,
        0,
        0,
        5,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 10,
      "input": [
        0,
        0,
        0,
        0,
        1,
        0,
        0,
        2,
        0
      ],
      "output": [
        0,
        0,
        4,
        0,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 11,
      "input": [
        0,
        0,
        0,
        0,
        3,
        0,
        0,
        1,
        0
      ],
      "output": [
        0,
        3,
        3,
        3,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 12,
      "input": [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ],
      "output": [
        2,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ]
    }
  ],
  "initialState": [
    6,
    0,
    0,
    1,
    4,
    0,
    0,
    -3,
    0
  ],
  "desiredState": [
    0,
    26,
    35,
    26,
    0,
    0,
    0,
    0,
    0
  ],
  "everyTurnRule": [
    0,
    0,
    0,
    0,
    4,
    0,
    0,
    0,
    0
  ],
  "turns": 6,
  "commands": 4,
  "options": {
    "drift": {
      "minimum": 0,
      "maximum": 0,
      "isReset": false
    },
    "heat": {
      "minimum": 0,
      "maximum": 3
    },
    "electricity": {
      "minimum": 1
    }
  }
}
// describe('final quiz', ( ) => {
//   it('solve it ', () => {
//
//   });
// });

function main( ) {
  // data.turns = 3;
  const solver = new XxlSolver(data.productionRules,
      data.initialState,
      data.desiredState,
      data.everyTurnRule,
      data.turns,
      data.commands,
      data.options,
  );
  const solutions = solver.run();

  console.log({solutions});
  const solution = SolverSolutionTransformer.findBestSolution(solutions);
  console.log({solution});
  const transform = SolverSolutionTransformer.transform(solution, data.commands);
  console.log({transform});
}

main();
