const path = require('path');
//const webpack = require('webpack');

module.exports = {

  entry: './src/xxl-solver-final.test.js',
  target: 'node',
  output: {
    path: path.resolve(__dirname, 'dist'),
    'filename': 'profile.js',
  },
  mode: "none",
  // watch: true,
  resolve: {
    'extensions': ['.js'],
  },
  /*
  plugins: [
    new webpack.SourceMapDevToolPlugin({}),
  ],
  eval-cheap-module-source-map
  */
  devtool: 'eval-cheap-module-source-map',
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: [
              '@babel/plugin-proposal-class-properties',
              '@babel/plugin-transform-runtime'
            ],
          },
        },
      },
    ],
  },
  optimization:{
    minimize: false
  }
};
