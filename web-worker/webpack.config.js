const path = require('path');
const webpack = require('webpack');

module.exports = {

  entry: './src/web-worker.js',
  target: 'web',
  output: {
    path: path.resolve(__dirname, '../public'),
    'filename': 'web-worker.bundle.js',
  },
  mode: "none",
  // watch: true,
  resolve: {
    'extensions': ['.js'],
  },
  plugins: [
    new webpack.SourceMapDevToolPlugin({}),
  ],
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: [
              '@babel/plugin-proposal-class-properties',
              '@babel/plugin-transform-runtime'
            ],
          },
        },
      },
    ],
  },

};
