import XxlSolver from '../../src/solver/xxl-solver.js';

console.log('WebWorker');

export class SolverWorker {
  constructor() {
    self.onmessage = async (event) => {
      console.log({data: event.data});
      const result = this.run(event.data.payload, event.data.commands);
      postMessage(result, []);
    };
  }

  run(payload, commands) {
    const solver = new XxlSolver(
        payload.productionRules,
        payload.initialState,
        payload.desiredState,
        payload.everyTurn,
        payload.turns,
        payload.commands,
        payload.config
    );
    if (commands.length === 0) {
      return solver.run();
    } else {
      return solver.runWithCommands(commands);
    }
  }
}

function start() {
  const worker = new SolverWorker();
  console.log('Worker started');
}

start();

