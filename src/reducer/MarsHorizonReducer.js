import {createSlice} from '@reduxjs/toolkit';
import {generate} from '../model/ResourceMap';
import MarsHorizonState from '../model/MarsHorizonState';

/**
 *
 * @type {{valueHolders: {
 * '/root/solver': boolean,
 * '/root/turns': number,
 * '/root/commands': number,
 * '/root/requiredCommands': string},
 * rules: {
 * '-1': {output: {resourceId: string, emoji: string, counter: number}[], id: number},
 * '-2': {output: {resourceId: string, emoji: string, counter: number}[], id: number},
 * '-3': {output: {resourceId: string, emoji: string, counter: number}[], id: number}}}}
 */
export const initialState = {
  rules: {
    '-1': {id: -1, output: generate(0)},
    '-2': {id: -2, output: generate(0)},
    '-3': {id: -3, output: generate(0)},

  },
  valueHolders: {
    '/root/turns': 4,
    '/root/commands': 3,
    '/root/solver': true,
    '/root/requiredCommands': '',
    '/root/solver/heat/minimum': 0,
    '/root/solver/heat/maximum': 3,
    '/root/solver/drift/minimum': -1,
    '/root/solver/drift/maximum': 1,
    '/root/solver/drift/isReset': false,
    '/root/solver/electricity/minimum': 1,
  },
};
const MarsHorizonSlice = createSlice(
    {
      name: 'MarsHorizonSlice',
      initialState: initialState,
      reducers: {
        increment: (state, {payload}) =>
            MarsHorizonState.increment(state, payload.ruleId, payload.section,
            payload.resourceId),
        decrement: (state, {payload}) =>
            MarsHorizonState.decrement(state, payload.ruleId, payload.section,
            payload.resourceId),
        updateRule: (state, {payload}) =>
            MarsHorizonState.update(state, payload.ruleId, payload.section,
            payload.resourceId, payload.delta),
        createRule: (state) => MarsHorizonState.createRule(state),
        deleteRule: (state, {payload}) =>
            MarsHorizonState.deleteRule(state, payload.ruleId),
        runSolver: (state, {payload}) =>
            MarsHorizonState.runSolver(state, payload),
        updateNumericValueHolder: (state, {payload}) =>
            MarsHorizonState.updateValueHolder(state, payload.valueHolder,
                payload.delta),
        clearAll: (state, {payload}) => initialState,
        clearSolution: (state, {payload}) => {
          state.valueHolders['/root/solution'] = undefined;
          return state;
        },
        useAsNewState: (state, {payload}) =>
            MarsHorizonState.useAsNewState(state, payload),
        toggleBooleanValueHolder: (state, {payload}) =>
            MarsHorizonState.toggleBooleanValueHolder(state,
                payload.valueHolder),
        setValueHolder: (state, {payload}) =>
            MarsHorizonState.setValueHolder(state, payload.valueHolder,
                payload.value),
      },
    },
);

export const {
  clearAll,
  clearSolution,
  createRule,
  decrement,
  deleteRule,
  increment,
  runSolver,
  updateNumericValueHolder,
  setValueHolder,
  updateRule,
  useAsNewState,
  toggleBooleanValueHolder,
} = MarsHorizonSlice.actions;

export default MarsHorizonSlice;

