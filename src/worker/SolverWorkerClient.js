class SolverWorkerClient {


  constructor() {
    this.worker = new Worker('./web-worker.bundle.js');
    this.worker.onmessage = (event) => {
      console.log('message from worker');
      console.log(event);
      this.currentCallback(event);
    }


  }

  run(payload, commands, callback) {
    this.currentCallback = callback;
    return this.worker.postMessage({payload, commands});
  }
}

export default SolverWorkerClient;
