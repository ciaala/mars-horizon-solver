import {Chart} from 'react-charts';
import data from '../../benchmark_data/strange increment.benchmark_data.json';

function BenchmarkCharts() {
  const series = {};
  const axes = {};
  return <Chart data={data} series={series} axes={axes} tooltip/>;
}

export default BenchmarkCharts;