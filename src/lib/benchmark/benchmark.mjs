import {performance, PerformanceObserver} from 'perf_hooks';

const measurements = new Map();

const obs = new PerformanceObserver(
    (items) => {

        const entries = items.getEntries();
        // console.log({entries});
        //  console.log(entries[0].duration);
        performance.clearMarks();
        measurements.set(entries[0].name, entries[0].duration);
        //  console.log(measurements);
    }
);
obs.observe({entryTypes: ['measure']});

function printTimingMarker(repetition, timerName, time) {
    console.info(`The '${timerName}' has run #${repetition} repetition in ${time} with ${repetition / time} op/s`);
}

function _benchmarkFunction(repetition, fn, timerName = 'Repetition', shouldConsoleLogTimer) {

    const originalRepetition = repetition;
    performance.mark("start" + timerName);
    while (repetition--) {
        fn();
    }
    performance.mark("end" + timerName);

    performance.measure(
        timerName,
        'start' + timerName,
        'end' + timerName);
    const time = measurements.get(timerName);
    if (shouldConsoleLogTimer) {
        printTimingMarker(originalRepetition, timerName, time);
    }
    return time;
}

function trackData(timerName, time, repetition) {
    return {
        timer: timerName,
        repetition: repetition,
        time,
        ops: repetition / time
    };
}

function benchmark({repetition = 40, warmup = 5, repeat = 40, shouldConsoleLogTimer = false}, fn) {

    let i = 0
    const data = new Array(repeat);

    const runConfigurations = {
        "Warmup": warmup,
        "Repetition": repetition,
        "Repetition x10": repetition * 10,
    };
    const timers = Object.keys(runConfigurations);

    timers.filter(key => runConfigurations[key] > 0);

    while (i < repeat) {
        data[i] = new Array(timers.length);
        for (let k = 0; k < timers.length; k++) {
            const timerName = timers[k];
            const time = _benchmarkFunction(runConfigurations[timerName], fn, timerName, shouldConsoleLogTimer);
            data[i][k] = trackData(timerName, time, runConfigurations[timerName]);
        }
        i++;
        performance.clearMarks();
    }
    return data;
}

const local = {value: 1};

const runData = benchmark('how long does it takes to solve a solution',
    {repetition: 1000, warmup: 5, shouldConsoleLogTimer: false},
    () => {
        local.value = Math.pow(2, local.value % 16);
    }
);

export default benchmark;
