import benchmark from './benchmark.mjs';
import file_benchmark from './file';
import {readFileSync} from 'fs';

describe('can write benchmark data to file', () => {
  it('support writing benchmark to file', () => {
        const data = benchmark({}, () => {
              for (let i = 1; i < 10; i++) {
                i += i / (Math.random() / 2);
              }
            },
        );

        file_benchmark('./output/benchmark_data',
            {name: 'strange increment', data});
        let raw = readFileSync(
            './output/benchmark_data/' + 'strange increment' + '.benchmark_data.json');
        let reloaded_data = JSON.parse(raw);
        expect(reloaded_data).toStrictEqual(data);
      },
  );
});