import {mkdirSync, writeFileSync} from 'fs';

function file_benchmark(output_folder, {name, data}) {

  const outputFile = output_folder + '/' + name + '.benchmark_data.json';
  mkdirSync(output_folder,
      {recursive: true},
  );

  const jsonContent = JSON.stringify(data,undefined, 2);
  writeFileSync(outputFile,
      jsonContent,
      'utf8',
  );
}

export default file_benchmark;
