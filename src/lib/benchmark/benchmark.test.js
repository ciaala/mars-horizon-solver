import benchmark from './benchmark.mjs';

const DATA_IN_KEYS = 3;

describe('the benchmark framework calls and collect timing about the function it accepts', () => {
    const repeat = 2;
    const conf = {repetition: 10, warmup: 1, repeat: repeat};

    let countRuns = 0;

    const data = benchmark(conf,
        () => {

            countRuns++;
        });

    it('the function is called', () => {
        expect(countRuns).toBe(repeat * (100 + 10 + 1));
    });

    it('the function returns the timing data', () => {

        expect(data.length).toBe(repeat);
        for (let i = 0; i < repeat; i++) {
            for (let k = 0; k < DATA_IN_KEYS; k++) {
                expect(data[i][k]).toHaveProperty('timer');
                expect(data[i][k]).toHaveProperty('time');
                expect(data[i][k]).toHaveProperty('repetition');
                expect(data[i][k]).toHaveProperty('ops');
            }
        }
    });

    it('the timing data make sense', () => {

        for (let i = 0; i < repeat; i++) {
            for (let k = 0; k < DATA_IN_KEYS; k++) {
                expect(data[i][k].time).toBeGreaterThan(0);
            }
        }
        // 'Repetition x 10' are always bigger than 'repetition'
        for (let i = 0; i < repeat; i++) {
            expect(data[i][2].time).toBeGreaterThan(data[i][1].time);
        }
    });
})

