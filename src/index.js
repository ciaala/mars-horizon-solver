import React from 'react';
import ReactDOM from 'react-dom';
import './fonts/2K4sRegular-r1oB.ttf';
import GlobalApp from './app/GlobalApp';

const htmlElement = document.getElementById('root');
ReactDOM.render(
    <React.StrictMode>
      <GlobalApp/>
    </React.StrictMode>,
    htmlElement);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

