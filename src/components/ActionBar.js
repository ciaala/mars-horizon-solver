import React from 'react';
import styles from './MarsHorizon.module.css';
import * as PropTypes from 'prop-types';

export default function ActionBar(props) {
  return <div className={styles.ActionBar}>{props.children}</div>
}