import styles from './MarsHorizon.module.css';
import * as PropTypes from 'prop-types';
import ResourceSet from './ResourceSet';
import React from 'react';
import {Twemoji} from 'react-emoji-render';
import {connect} from 'react-redux';
import {deleteRule} from '../reducer/MarsHorizonReducer';

function ProductionRule(props) {
  const rule = props.rule;
  const removeHandler = (event) => {

    const p = deleteRule({ruleId: rule.id});
    console.log('RemoveHandler', p);
    props.dispatch(p);
    event.preventDefault();
  };
  return <div className={styles.ProductionRule}>
    <Twemoji className={styles.ProductionArrow} svg text={':heavy_minus_sign:'}
             onClick={(event) => removeHandler(event)}/>
    <span className={styles.Index}>{rule.id}</span>

    <ResourceSet resources={rule.input}
                 ruleId={rule.id}
                 section={'input'}/>
    <Twemoji className={styles.ProductionArrow} svg text={':arrow_right:'}/>
    <ResourceSet resources={rule.output}
                 ruleId={rule.id}
                 section={'output'}/>


  </div>;
}

ProductionRule.propTypes = {
  rule: PropTypes.object,
};

export default connect()(ProductionRule);
