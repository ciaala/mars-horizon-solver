import * as PropTypes from "prop-types";
import styles from "./MarsHorizon.module.css";
import {Twemoji} from "react-emoji-render";
import {useDispatch, useSelector} from "react-redux";
import {toggleBooleanValueHolder} from "../reducer/MarsHorizonReducer";

function clickHandlerFactory(dispatch, payload) {
    return ((event) => {
        dispatch(toggleBooleanValueHolder(payload));
        event.preventDefault();
    });
}


function ToggleValueHolder(props) {
    const value = useSelector((state) => state.valueHolders[props.valueHolder]);
    const dispatch = useDispatch();
    const mouseClickHandler = clickHandlerFactory(dispatch,
        {valueHolder: props.valueHolder});
    const shaded = !value ? styles.shaded : '';
    return <div className={styles.TableExtraRow}>
        <span className={styles.Index}>{props.title}</span>
        <Twemoji svg className={styles.Emoji + " " + shaded}
                 text={props.emoji}
                 onMouseDown={mouseClickHandler}
                 onContextMenu={(e) => e.preventDefault()}/>
    </div>;
}

ToggleValueHolder.propTypes = {
    emoji: PropTypes.string,
    title: PropTypes.string,
    valueHolder: PropTypes.string
};


export default ToggleValueHolder;