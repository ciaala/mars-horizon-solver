import styles from './MarsHorizon.module.css';
import React from 'react';
import {Twemoji} from 'react-emoji-render';
import * as PropTypes from 'prop-types';

/**
 * @typedef {object} ResourceItem
 * @property {string} emoji
 * @property {number} counter
 */


function createMouseClickHandler(payload, onPrimaryAction, onSecondaryAction) {
  return (event) => {
    if (event.button === 0) {
      onPrimaryAction(event, payload);
    } else if (event.button === 2) {
      onSecondaryAction(event, payload);
    }
    event.preventDefault();
  };
}

function Counter(props) {
  const resource = props.resource;
  const payload = {ruleId: props.ruleId, section: props.section, resourceId: resource.resourceId};
  const clickHandler = (props.onPrimaryAction || props.onSecondaryAction)
      ? createMouseClickHandler(payload, props.onPrimaryAction, props.onSecondaryAction)
      : undefined;
  const shaded = resource.counter === 0 ? styles.shaded : '';
  if (!resource.emoji) {
    console.log("Missing emoji");
    resource.emoji = ':pleading_face:';
  }
  return <span className={shaded}>
          <Twemoji svg
                   className={styles.Emoji}
                   text={resource.emoji}
                   onMouseDown={clickHandler}
                   onContextMenu={e => e.preventDefault()}/>
          <span className={styles.Counter}>{resource.counter}</span>
        </span>;
}

Counter.propTypes = {
  resource: PropTypes.object,
  onSecondaryAction: PropTypes.func,
  onPrimaryAction: PropTypes.func,
  ruleId: PropTypes.number,
};

export default Counter;
