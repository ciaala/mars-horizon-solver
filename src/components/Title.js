import styles from "./MarsHorizon.module.css"
import React from 'react';

export default function Title(props) {
  return <h1 className={styles.Title}>{props.text}</h1>;
}