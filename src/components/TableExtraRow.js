import {connect} from 'react-redux';
import styles from './MarsHorizon.module.css';
import ResourceSet from './ResourceSet';
import React from 'react';

function TableExtraRow(props) {

  const resources = props.rules[props.ruleId][props.section];
  return (
      <div className={styles.TableExtraRow}>
        <span className={styles.Index}>{props.title}</span>
        <ResourceSet ruleId={props.ruleId} section={props.section} resources={resources}/>
      </div>
  );
}

const mapStateToProps = (state) => {
  return {rules: state.rules};
};
export default connect(mapStateToProps)(TableExtraRow);