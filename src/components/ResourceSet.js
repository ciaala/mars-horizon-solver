import React from 'react';
import styles from './MarsHorizon.module.css';
import * as PropTypes from 'prop-types';
import Counter from './Counter';
import {useDispatch} from 'react-redux';
import {updateRule} from '../reducer/MarsHorizonReducer';

Counter.propTypes = {
  resource: PropTypes.any,
};

/**
 *
 * @param dispatch
 * @param amount
 * @returns {function(*, *=): void}
 */
function createUpdateHandler(dispatch, amount) {
  return (event, payload) => {
    payload.delta = amount * (event.shiftKey ? 5 : 1);
    dispatch(updateRule(payload));
  };
}

/**
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
function ResourceSet(props) {
  //console.log('ResourceSet', props);
  const dispatch = useDispatch();

  const onPrimaryAction = createUpdateHandler(dispatch, 1);
  const onSecondaryAction = createUpdateHandler(dispatch, -1);
  let children;
  if (props.resources) {
    children = props.resources.map(resource => {
      return <Counter key={resource.resourceId}
                      resource={resource}
                      onPrimaryAction={onPrimaryAction}
                      onSecondaryAction={onSecondaryAction}
                      ruleId={props.ruleId}
                      section={props.section}
      />;
    });
  } else {
    children = [];
  }
  return <span className={styles.ResourceSet}>{children}</span>;
}

ResourceSet.propTypes = {
  ruleId: PropTypes.number,
  section: PropTypes.string,
  resources: PropTypes.arrayOf(PropTypes.object),

};
export default ResourceSet;