import * as PropTypes from 'prop-types';
import React from 'react';
import {Twemoji} from 'react-emoji-render';
import {useAsNewState} from '../reducer/MarsHorizonReducer';
import Counter from './Counter';
import styles from './MarsHorizon.module.css';
import ResourceMap, {generate} from '../model/ResourceMap';
import Title from './Title';
import {useDispatch} from 'react-redux';

const RESOURCE_ENTRIES = Object.entries(ResourceMap);

/**
 * @typedef {Object} Resource
 * @property {string} resourceId
 * @property {number} counter
 * @property {string} emoji
 */

/**
 * @typedef {Object} MarsRule
 * @property {number} id
 * @property {Resource[]} input
 * @property {Resource[]} output
 */

/**
 *
 * @param {Resource[]} {sequence}
 * @returns {JSX.Element}
 * @constructor
 */

function ResourceSequence({sequence}) {
  //console.log('ResourceSequence', sequence);
  const emojiResources = sequence//
      .filter(r => r.counter !== 0) //
      .map((r, i) => <Counter key={i} resource={r}/>);
  return <div className={styles.ResourceSequence}>{emojiResources}</div>;
}

function addClickHandler(dispatch, resourceState) {
  return (event) => {
    event.preventDefault();
    const action = useAsNewState(resourceState);
    dispatch(action);
  };
}

/**
 *
 * @param {MarsRule} {command}
 * @returns {JSX.Element}
 * @constructor
 */
function Command({command}) {
  //console.log(command);
  return <div className={styles.Command}>
    <span className={styles.Index}>{command.id}</span>
    <ResourceSequence sequence={command.input}/>
    <Twemoji className={styles.ProductionArrow} svg text={':arrow_right:'}/>
    <ResourceSequence sequence={command.output}/>
  </div>;
}

/**
 *
 * @param {number[]} props.commands
 * @param {number[]} props.state
 * @param {number} props.index
 * @param {Map<String,MarsRule>} props.rules
 * @returns {JSX.Element}
 * @constructor
 */
function Turn(props) {
  const dispatch = useDispatch();
 // console.log('Turn', props);
  const ruleArray = Object.values(props.rules);
  const exploded = props.commands.map(
      c => ruleArray.filter(r => r.id === c)[0]);
 // console.log('Turn', {exploded});
  const commands = exploded.map(
      (command, i) => <Command key={i} command={command}/>);

  const resourceState = props.state.map((c, i) => {
        const resource = RESOURCE_ENTRIES[i];
        return {
          counter: c,
          resourceId: resource[0],
          emoji: resource[1],
        };
      },
  );
 // console.log({exploded, commands, resourceState});
  const diff = [];
  try {
    const quality = exploded.reduce((acc, c) => {
      c.input.forEach((v, i) =>
          acc[i].counter -= v.counter);
      c.output.forEach((v, i) =>
          acc[i].counter += v.counter);
      return acc;
    }, generate(0));
    quality.forEach(e => diff.push(e));
  } catch (e) {
    console.error(e);
    window.alert('invalid solution data, regenerate it!');
    return <span>ERROR !!!</span>;
  }

  return <div>
    <div className={styles.Turn}>
      <Title text={props.index}/>
      <div className={styles.TurnData}>
        {commands}
        <div className={styles.PartialState}>
          <Command command={{id: '', input: diff, output: resourceState}}/>
          <Twemoji className={styles.SwitchState} svg
                   text={':twisted_rightwards_arrows:'}
                   onMouseDown={addClickHandler(dispatch, resourceState)}
          />
        </div>
      </div>
    </div>
  </div>;
}

Turn.propTypes = {
  commands: PropTypes.arrayOf(PropTypes.number),
  state: PropTypes.arrayOf(PropTypes.number),
  rules: PropTypes.objectOf(PropTypes.object),
  index: PropTypes.number,
};

export default Turn;
