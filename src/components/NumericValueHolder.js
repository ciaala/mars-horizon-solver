import {Twemoji} from 'react-emoji-render';
import styles from './MarsHorizon.module.css';
import {useDispatch, useSelector} from 'react-redux';
import {updateNumericValueHolder} from '../reducer/MarsHorizonReducer';
import * as PropTypes from 'prop-types';

export function clickHandlerFactory(dispatch, payload) {
  return ((event) => {
    console.log("clickHandler", {dispatch,payload});
    let delta = 0;
    if (event.button === 0) {
      delta = 1;
    } else if (event.button === 2) {
      delta = -1;
    }
    payload.delta = delta * (event.shiftKey ? 5 : 1);
    dispatch(updateNumericValueHolder(  payload));
    event.preventDefault();
  });
}

function NumericValueHolder(props) {
  const value = useSelector((state) => state.valueHolders[props.valueHolder]);
  const dispatch = useDispatch();
  const mouseClickHandler = clickHandlerFactory(dispatch,
      {valueHolder: props.valueHolder});
  return (
      <div className={styles.TableExtraRow}>
        <span className={styles.Index}>{props.title}</span>
          <Twemoji svg className={styles.Emoji}
                   text={props.emoji}
                   onMouseDown={mouseClickHandler}
                   onContextMenu={e => e.preventDefault()}/>
                   <span className={styles.Counter}>{value}</span>
      </div>
  );
}

NumericValueHolder.propTypes = {
  valueHolder: PropTypes.string,
  emoji: PropTypes.string,
};

export default NumericValueHolder;
