import React from 'react';
import {Twemoji} from 'react-emoji-render';
import styles from './MarsHorizon.module.css';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';

/**
 *
 * @param {string} props.emojiIdentifier
 * @param {function} props.onClick
 * @param {function} props.dispatch
 * @returns {JSX.Element}
 * @constructor
 */
function Action(props) {
  const clickHandler = e => {
    e.preventDefault();
    if (!props.shouldConfirm || window.confirm(props.confirmMessage)) {
      const p = props.onClick({dispatch: props.dispatch});
      props.dispatch(p);
    }
  };
  return <Twemoji className={styles.Action}
                  text={props.emojiIdentifier}
                  onClick={clickHandler}
                  onContextMenu={e => e.preventDefault()}/>;
}

Action.propTypes = {
  onClick: PropTypes.func,
  emojiIdentifier: PropTypes.string,
};
export default connect()(Action);
