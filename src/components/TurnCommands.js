import {useSelector} from 'react-redux';
import React from 'react';
import * as PropTypes from 'prop-types';
import styles from './MarsHorizon.module.css';

import Turn from './Turn.js';

/**
 *
 * @param {string} props.selector
 * @returns {JSX.Element}
 * @constructor
 */
function TurnCommands(props) {

  const {allRules, solution} = useSelector(state => ({
    solution: state.valueHolders[props.selector],
    allRules: state.rules,
  }));

  /**
   *
   * @type {SolverRule[]}
   */
  const arrayRules = Object.entries(allRules)//
      .filter(([id, r]) => id >= 0) //
      .map((e, i) => [i, e[1]]);

  const rules = Object.fromEntries(arrayRules);

  /**
   *
   * @type {Array.<
   *        {commands: Array.<number>, state: Array.< number>}
   *      >}
   */
  const turnsData = solution ? solution : [];
  console.log({turnsData, rules});
  if (turnsData.length > 0 && rules.length === 0) {
    return <span className={styles.ErrorPlaceHolder}>Unable to show the Solution because it is missing lines</span>;
  }
  try {
    const turnComponents = turnsData.map(
        ({commands, state}, i) =>
            <Turn key={i}
                  index={i}
                  commands={commands}
                  rules={rules}
                  state={state ? state : []}
            />);
    return <div className={styles.TurnList}>{turnComponents}</div>;
  } catch (e) {
    console.error(e);
    return <span>"Unable to render the current state{e.toString()}</span>;
  }
}

TurnCommands.propTypes = {
  selector: PropTypes.string,
}
;
export default TurnCommands;
