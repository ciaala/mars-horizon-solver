import React from 'react';
import ProductionRule from './ProductionRule';
import * as PropTypes from 'prop-types';
import styles from './MarsHorizon.module.css';
import {connect} from 'react-redux';

/**
 * @typedef {object} ResourceItem
 * @property {string} emoji
 * @property {number} counter
 */

/**
 * @typedef {object} ProductionRuleType
 * @property {Array.<ResourceItem>} input
 * @property {Array.<ResourceItem>} output
 */
/**
 *
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */

function ProductionRuleTable(props) {
  console.log('ProductionRuleTable', props);
  /**
   *
   * @type {Array.<ProductionRuleType>}
   */
  const rules = props.rules;
  const children = Object.entries(rules)
  .filter(([key,value]) => 0 <= key)
  .map(([key, value]) =>
          <ProductionRule
              key={key}
              rule={value}
          />);

  return <div className={styles.ProductionRuleTable}>
    {props.children}
    {children}</div>;
}

ProductionRuleTable.propTypes = {
  rules: PropTypes.object,
};
const mapStateToProps = state => ({rules: state.rules});

export default connect(mapStateToProps)(ProductionRuleTable);
