import * as PropTypes from 'prop-types';
import {useDispatch, useSelector} from 'react-redux';
import AsRow from '../layout/AsRow';
import {clickHandlerFactory} from './NumericValueHolder';
import Title from './Title';
import {Twemoji} from 'react-emoji-render';
import styles from './MinMaxRange.module.css';

function ClickNumber(props) {
  const dispatch = useDispatch();
  const mouseClickHandler = clickHandlerFactory(dispatch,
      {valueHolder: props.valueHolder});

  return <div
      onMouseDown={mouseClickHandler}
      onContextMenu={e => e.preventDefault()}>
    {props.children}
  </div>;
}

ClickNumber.propTypes = {
  selector: PropTypes.string,
  children: PropTypes.node,
};

function MinMaxRange(props) {
  const [min, max] = useSelector(state => [
    state.valueHolders[props.minValueHolder],
    state.valueHolders[props.maxValueHolder]
  ]);

  return <div className={styles.MinMaxRange}>
    <AsRow>
      <span className={styles.Label}>{props.title}</span>
      <ClickNumber valueHolder={props.minValueHolder}>
        <span className={styles.MinValue}>{min}</span>
      </ClickNumber>
      <Twemoji svg
               text={props.emoji}
               onContextMenu={e => e.preventDefault()}
      />
      <ClickNumber valueHolder={props.maxValueHolder}>
        <span className={styles.MaxValue}>{max}</span>
      </ClickNumber>
    </AsRow>
  </div>;
}

MinMaxRange.propTypes = {
  minValueHolder: PropTypes.string,
  maxValueHolder: PropTypes.string,
  emoji: PropTypes.string,
  title: PropTypes.string,
}
;
export default MinMaxRange;
