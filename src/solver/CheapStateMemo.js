export default class CheapStateMemo {
  /**
   *
   * @type {Map<string, Map<string, Int8Array>>}
   */
  first8bytes = new Map();

  constructor() {

  }

  /**
   *
   * @param {Int8Array} state
   */
  createOrRetrieve(state) {
    // Minimal Length => all 1 decimal 10+9 [19]
    // Reasonable Length => 3 elements at 2 decimal the remaining at 1 decimal [22]
    //
    const serialized = state.toString();
    // prefix contains
    // electricity => [2]
    // signal, navigation => [4] [6]
    // data optionally at [0],[2]

    const prefix = serialized.substring(0, 8);
    const suffix = serialized.substring(8);
    if (!this.first8bytes.has(prefix)) {
      this.first8bytes.set(prefix, new Map());
    }
    const followingMap = this.first8bytes.get(prefix);
    if (!followingMap.has(suffix)) {
      followingMap.set(suffix, state);
      return state;
    } else {
      return followingMap.get(suffix);
    }
  }
}
