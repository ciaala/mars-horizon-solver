import {nextCommandSet} from './solver';

const n3 = [
  0, 0, 0,
];
const n2 = [
  0, 0,
];
/**
 *
 * @type {number[][]}
 */
const electricityRule = [
  n2, n3, [
    1, 0,
  ], n3,
];
/**
 *
 * @type {number[][][]}
 * @private
 */
const _productionRules = [
  [
    [
      1, 0,
    ], n3, [
    2, 0, 0,
  ],
  ], [
    n2, [
      0, 2, 0,
    ], [
      2, 0, 2,
    ],
  ], [
    n2, [
      0, 1, 1,
    ], [
      4, 0, 0,
    ],
  ],

  [
    [
      1, 0,
    ], n3, [
    0, 3, 0,
  ],
  ], [
    n2, [
      0, 0, 2,
    ], [
      0, 3, 0,
    ],
  ], [
    n2, [
      3, 0, 0,
    ], [
      0, 3, 2,
    ],
  ],

  [
    [
      1, 0,
    ], n3, [
    0, 0, 2,
  ],
  ], [
    n2, [
      0, 1, 0,
    ], [
      1, 0, 2,
    ],
  ], [
    [
      1, 0,
    ], [
      1, 0, 0,
    ], [
      0, 0, 4,
    ],
  ],

  [
    [
      0, 2,
    ], n3, [
    4, 0, 0,
  ],
  ], [
    [
      0, 1,
    ], [
      2, 0, 0,
    ], [
      0, 5, 0,
    ],
  ], [
    [
      1, 1,
    ], [
      0, 0, 0,
    ], [
      2, 1, 2,
    ],
  ],
];

/**
 *
 * @param {number[][]} newState
 * @param {number} commandTurns
 * @returns {boolean}
 */
function isElectricityCool(newState, commandTurns) {
  const minimumElectricity = commandTurns / 2;
  const isHigh = newState[0][0] >= minimumElectricity;
  return isHigh;
}

/**
 * @typedef {Object} TurnCommands}
 * @property {number[]} newState
 * @property {number[]} commands
 */

/**
 *
 * @param {number[][]} newState
 * @param {number[][]} p
 */

function updateStateWithProductionRule(newState, p) {
  const debugThis = false;
  let original;
  if (debugThis) {
    original = JSON.parse(JSON.stringify(newState));
  }
  newState.forEach((e, i, ar) => {
        e.forEach(
            (ee, ii, arr) => {
              arr[ii] = ee - p[i][ii] + p[i + 2][ii];
            });
      },
  );
  if (debugThis) {
    console.log({original, newState, p});
  }
}

/**
 *
 * @param {number} commandTurns
 * @param {number[]} commandExplorer
 * @param {number[][][]} productionRules
 * @param {number[][]} newState
 * @returns {boolean}
 */
function canRunSequenceOfCommands(
    commandTurns, commandExplorer, productionRules, newState) {
  for (let i = 0; i < commandTurns; i++) {
    const iCommand = commandExplorer[i];
    const p = productionRules[iCommand];
    updateStateWithProductionRule(newState, p);
    const canRunCommand = canConsume(newState[0], p[0])
        && canConsume(newState[1], p[1])
        && isElectricityCool(newState, commandTurns);
    if (!canRunCommand) {
      return false;
    }
  }
  return true;
}

/**
 * return the selected commands, the newState
 * @param {number[]} state
 * @param {number[]} commandExplorer
 * @param {number} commandTurns
 * @param {number[][][]} productionRules
 * @return {TurnCommands}
 */
function generateTurnCommands(
    state, commandExplorer, {commandTurns, productionRules}) {

  let found = true;

  const commandIndexLimit = productionRules.length - 1;

  while (found !== undefined) {
    found = nextCommandSet(commandExplorer, commandIndexLimit, commandTurns);
    if (found) {
      const newState = [state[0].slice(), state[1].slice()];
      const canExecuteCommands = canRunSequenceOfCommands(
          commandTurns,
          commandExplorer,
          productionRules,
          newState);
      if (canExecuteCommands) {
        return {
          newState,
          'commands': commandExplorer.slice(),
        };
      }
    }
  }
  return undefined;
}




/**
 *
 * @param {number[][][]} productionRules
 * @param {number} turns
 * @param {number} commandTurns
 * @param {number[][]} initialState
 * @param {Array.<number>} requirements
 */
function runBruteForceSolution(
    productionRules,
    [turns, commandTurns],
    initialState,
    requirements) {
  const turnSolutions = [];
  // production -> commands
  // const memo = new Map()

  let currentState = initialState.slice();

  // const tryCommands = []
  //  const indexCommands = []
  const ruleConfig = {commandTurns, 'productionRules': productionRules};

  for (let turn = 0; turn < turns; turn++) {
    console.log('Turn ' + turn);
    let bestScore = LOWEST_SCORE;
    let commandExplorer = new Array(commandTurns).fill(
        productionRules.length - 1);
    commandExplorer[0] = -1;
    let notFinish = true;
    /**
     *
     * @type {TurnCommands}
     */
    let best = null;
    const totalMoves = Math.pow(commandTurns, productionRules.length);
    let move = 0;
    while (notFinish && move < totalMoves) {
      // console.log(move)
      const result = generateTurnCommands(
          [currentState[0].slice(), currentState[1].slice()], commandExplorer,
          ruleConfig);
      if (result !== undefined) {
        const score = scoreResourcesInNewState(currentState[1],
            result.newState[1], requirements);
        if (score > bestScore) {
          best = {score, ...result, move};
          bestScore = score;
          console.log(JSON.stringify({best, move}));
        }
      } else {
        notFinish = false;
      }
      move++;
    }
    if (best) {
      currentState = best.newState;
      printTurn(...best);
      turnSolutions.push(best);
    } else {
      return null;
    }
  }
  return {turnSolutions, productionRules};
}

function execute() {
  /**
   *
   * @type {number[]}
   */
  const requirements = [
    17, 17, 17,
  ];
  /**
   *
   * @type {number[][]}
   */
  const initialState = [[
    6, 2,
  ], [
    1, 0, 0,
  ]];
  findSolution(normalizeRules(_productionRules), [6, 4], initialState,
      requirements);
}

// execute();
if (false) {
  function test(commands) {
    const initialState = [[
      6, 2,
    ], [
      1, 0, 0,
    ]];
    canRunSequenceOfCommands(4,
        commands,
        normalizeRules(_productionRules),
        initialState);
  }

  test([[0, 3, 8, 5]]);
}



/**
 *
 * @param {number[][][]}productionRules
 * @returns {number[][][]}
 */
function normalizeRules(productionRules) {
  const rules = productionRules.map(e => [e[0], e[1], n2, e[2]]);
  rules.push(electricityRule);
  rules.forEach((e, i) => console.log(i, '\t', e));
  console.log();
  return rules;
}

