import {NumberOfResources} from '../model/ResourceMap';
import CheapStateMemo from './CheapStateMemo';
import {FastMemoState} from './fast-memo-state';
import {
  astronautIndex,
  driftIndex,
  electricityIndex,
  fireIndex,
  radiationIndex,
  rocketIndex,
  makeRule,
} from './utility-solver.test';

// TODO BABEL.MACRO
// #define MyARRAY Int8Array Int16Array

/**
 * @typedef {Object} SolverRule
 * @property {number} id
 * @property {Int8Array} input
 * @property {Int8Array} output
 */

/**
 * @typedef {Object} SolverResult
 * @property {number} move
 * @property {Int8Array} commands
 * @property {Int8Array[]} states
 */

/**
 * @typedef {Object} Explorer
 * @property {Int8Array} state
 * @property {Int8Array} commands
 */

class Explorer {

  /**
   * @type {Int8Array}
   */
  state;

  /**
   * @type {number|null}
   */
  command;

  /**
   * @type {Explorer|null}
   *
   */
  prev;

  /**
   *
   * @param {Int8Array} state
   * @param {number} command
   * @param {Explorer} prev
   */
  constructor(state, prev, command) {
    this.state = state;
    this.command = command;
    this.prev = prev;
  }

  /**
   * @returns {number[]} the commands used to reach this state
   */
  getAllCommands() {
    const commands = [];
    let current = this;
    while (current && current.command !== null && current.command !== undefined) {
      commands.unshift(current.command);
      current = current.prev;
    }
    return commands;
  }
}

/**
 *
 * @param {Int8Array} state
 * @param {SolverRule} rule
 * @returns {Int8Array}
 */
function generateNewStateFromRule(state, rule) {
  return state.map((e, index) =>
      e + rule.output[index] - rule.input[index],
  );
}

const END_OF_TURN_RULE = -3;

class XxlSolver {

  productionRules;
  initialState;
  desiredState;
  everyTurnRule;
  turns;
  commands;
  minimumElectricity;
  totalMoves;

  /**
   * @type {number}
   */
  maxNumberCommands;

  /**
   * State String -> State Int8Array
   * @type {Map.<String, Int8Array>}
   */
  memoState = new Map();
  cheapStateMemo = new CheapStateMemo();
  /**
   *
   * @type {FastMemoState}
   */
  fastMemoState = new FastMemoState();
  /**
   * State String -> State Int8Array
   * @type {Map.<Int8Array,number>}
   */
  memoStateTurn = new Map();

  /**
   * @type {number}
   */
  maximumElectricity;
  fireLowerLimit = 0;
  fireUpperLimit = 3;
  /**
   *
   * @type {number}
   */
  minimumDrift = 0;
  /**
   *
   * @type {number}
   */
  maximumDrift = 0;
  isDriftCenteredAtEndOfTurn = false;

  #isCollectingStates = false;
  #turnToStates = [];

  excludedFromNegativeCheck = [driftIndex, rocketIndex];

  /**
   * @type {Map.<number,SolverRule>}
   */
  productionRuleMap = new Map();
  /**
   * @type {SolverRule}
   */
  localEveryTurnRule;
  /**
   * @type {boolean}
   */
  hasRepeatedState;

  /**
   * @type {number[]}
   */
  ruleMaximumIntensity;
  /**
   * @type {Int8Array}
   */
  ruleIndexes;

  /**
   *
   * @returns {SolverResult[]}
   */
  /**
   *
   * @type {Map<number, number[]>}
   */
  maxRules = new Map();
  /**
   * @type {number[]}
   */
  limitedCommands;
  /**
   *
   * @type {boolean}
   */
  checkCanMakeAllResources = true;
  /**
   *
   * @type {boolean}
   */
  hasResetAstronaut;

  /**
   *
   * @param {{id: number, input:number[],output: number[]}[]} productionRules
   * @param {number[]} initialState
   * @param {number[]} desiredState
   * @param {number[]} everyTurnRule
   * @param {number} turns
   * @param {number} commands
   * @param {number} heat.minimum
   * @param {number} heat.maximum
   * @param {number} drift.minimum
   * @param {number} drift.maximum
   * @param {boolean} drift.isReset
   * @param {number} electricity.minimum
   */
  constructor(productionRules, initialState, desiredState, everyTurnRule,
              turns, commands,
              {
                drift,
                heat,
                electricity,
              }) {
    console.log('new XxlSolver()', {
      productionRules, initialState, desiredState, everyTurnRule,
      turns, commands,
      options: {
        drift,
        heat,
        electricity,
      },
    });
    this.productionRules = productionRules;

    this.minimumDrift = drift.minimum;
    this.maximumDrift = drift.maximum;
    this.isDriftCenteredAtEndOfTurn = drift.isReset;
    this.fireLowerLimit = heat.minimum;
    this.fireUpperLimit = heat.maximum;
    this.minimumElectricity = electricity.minimum;

    this.initialState = initialState;
    this.desiredState = desiredState;
    this.everyTurnRule = everyTurnRule;
    this.turns = turns;
    this.commands = commands;
    // = 0;
    //   Math.ceil(this.commands * this.turns * .15);
    this.maxNumberCommands = this.commands * this.turns + (this.turns - 1);
    this.totalMoves = Math.pow(this.productionRules.length,
        this.maxNumberCommands);
    this.maximumElectricity = Math.max(this.initialState[electricityIndex],
        this.turns * this.minimumElectricity);

    /**
     * it has repeated states because end of turn rule does not change anything
     * @type {boolean}
     */
    this.hasRepeatedState = everyTurnRule.every(e => e === 0);
    this.hasResetAstronaut = everyTurnRule[astronautIndex] > 0;
    this.localEveryTurnRule = this.makeRuleFromEndOfTurnRule();
    productionRules.forEach(e => this.productionRuleMap.set(e.id, e));
    this.productionRuleMap.set(this.localEveryTurnRule.id,
        this.localEveryTurnRule);

    this.prepareRuleMaximumIntensity();

    // Keep the indexes where the rule are positive
    this.ruleIndexes = this.desiredState.map((e, i) => e > 0 ? i : 0).filter(i => i > 0);

    console.log({
      'ruleIndexes': this.ruleIndexes,
      'ruleMaximumIntensity': this.ruleMaximumIntensity,
      'ruleMaximumResources': this.ruleMaximumResources,
    });
  }

  // TODO unused
  static trackFilter(collection, fn) {
    const caller = console.log((new Error()).stack.split('\n')[2].trim().split(' ')[1]);
    const result = collection.filter(fn);
    console.log('filter for: ' + caller + ', collection length ' + collection.length);
    return result;
  }

  prepareRuleMaximumIntensity() {
    const {maximumSum, maximumVector} = this.productionRules.reduce(
        ({maximumSum, maximumVector}, rule) => {
          const currentSum = rule.output.reduce(
              (sum, e, i) => {
                if (e > maximumVector[i]) {
                  maximumVector[i] = e;
                }
                sum += e;
                return sum;
              }, 0);
          maximumSum = currentSum > maximumSum ? currentSum : maximumSum;
          return {maximumSum, maximumVector};
        },
        {maximumSum: 0, maximumVector: new Array(NumberOfResources).fill(0)});
    this.ruleMaximumIntensity = maximumVector;
    this.ruleMaximumResources = maximumSum;
  }

  /**
   *
   * @param {number[]} commands
   * @returns {{states:Int8Array[], commands: Int8Array[], move: number}[]}
   */
  runWithCommands(commands) {
    this.limitedCommands = commands;
    return this.run();
  }

  /**
   *
   * @returns {{states:Int8Array[], commands: Int8Array[], move: number}[]}
   */
  run() {

    const solutions = [];
    const initial8State = this.makeInitialState();
    /**
     *
     * @type {Explorer}
     */
    const initialTurn = new Explorer(initial8State, null, null);

    // LOOP Pre Step
    let move = 0;
    let turn = 0;
    let workQueue = [initialTurn];

    while (solutions.length === 0 && turn < this.maxNumberCommands && workQueue.length !== 0) {

      console.log({turn}, workQueue.length);
      // workQueue[0].state[rocketIndex]
      const nextQueue = [];
      if (false && workQueue.length > 100 * 1000) {
        workQueue = workQueue.filter(e => this.canStillReachDesiredState(e.state, turn));
        console.log('pruned', {turn}, workQueue.length);
      }

      if (this.#isCollectingStates) {
        this.#turnToStates.push(workQueue.slice());
      }
      if (this.progressTrackingFunction) {
        this.progressTrackingFunction(turn, workQueue);
      }
      while (workQueue.length !== 0) {
        move++;
        const explorer = workQueue.pop();
        let newStates;
        if (this.isEndTurn(turn)) {
          newStates = this.updateStateWithEndTurnMove(explorer, turn);
        } else {
          newStates = this.calculateNextValidStates(explorer.state, this.productionRules, turn).
              filter(({rule, state}) => this.canStillReachDesiredState(state, turn)).
              filter(({rule, state}) => this.isStateImprovement(state, turn));
          newStates.forEach(({rule, state}) => this.trackState(state, turn));
        }

        for (const {state, rule} of newStates) {
          this.updateSolverWithNewState(
              explorer,
              rule,
              turn,
              state,
              move,
              solutions, nextQueue,
              initial8State);
        }
      }
      workQueue = nextQueue;
      turn++;
    }
    if (solutions.length === 0) {
      console.log('Not found');
    }
    return solutions;
  }

  updateStateWithEndTurnMove(explorer, turn) {
    let newStates;
    if (this.isValidRule(explorer.state, turn)) {
      newStates = this.calculateNextValidStates(explorer.state, [this.localEveryTurnRule], turn).
          map(({rule, state}) => {
            if (this.hasResetAstronaut) {
              state.fill(this.localEveryTurnRule.output[astronautIndex], astronautIndex, astronautIndex + 1);
            }
            if (this.isDriftCenteredAtEndOfTurn) {
              state.fill(0, driftIndex, driftIndex + 1);
            }
            if (state[rocketIndex] < 0) {
              state.fill(0, rocketIndex, rocketIndex + 1);
            }
            return {rule, state};
          }).
          filter(({rule, state}) => this.canStillReachDesiredState(state, turn));
      newStates.forEach(({rule, state}) =>
          this.trackState(state, turn));
    } else {
      newStates = [];
    }
    return newStates;
  }

  updateSolverWithNewState(explorer, rule, turn, state, move, solutions, nextQueue, initial8State) {

    // const commands = explorer.commands.slice();
    // commands.file(rule.id], turn);
    /*if (state !== explorer.state) {
      this.prevStateMap.set(state, explorer.state);
    }
     */
    const isSolution = this.isSolution(state);
    if (isSolution) {

      // TODO Rebuild
      const trimmedList = explorer.getAllCommands();
      trimmedList.push(rule.id);
   //   console.log({trimmedList});
      const turnCommands = {
        move,
        commands: new Int8Array(trimmedList),
        states: this.createStateHistory(initial8State, trimmedList),
      };
      //console.log('found solution', turnCommands);
      solutions.push(turnCommands);
    } else {
      nextQueue.push(new Explorer(state, explorer, rule.id));
    }
  }

  /**
   *
   * @param {Int8Array} initialState
   * @param {Int8Array} commands
   * @returns {Array.<Int8Array>}
   */
  createStateHistory(initialState, commands) {
    const {current: finalCurrent, states: finalStates} = commands.reduce(
        ({current, states}, c) => {
          const rule = this.productionRuleMap.get(c);
          const next = generateNewStateFromRule(current, rule);
          if (next[astronautIndex] > this.everyTurnRule[astronautIndex]) {
            next.fill(this.everyTurnRule[astronautIndex], astronautIndex, astronautIndex + 1);
          }
          if (next[rocketIndex] < 0) {
            next.fill(0, rocketIndex, rocketIndex + 1);
          }
          states.push(next);
          return {current: next.slice(), states};
        }, {current: initialState.slice(), states: [initialState]});
   // console.log(initialState, finalStates[finalStates.length - 1]);
    return finalStates.reverse();
  }

  makeInitialState() {
    return new Int8Array(this.initialState);
  }

  makeRuleFromEndOfTurnRule() {
    const localEveryTurnRule = makeRule(END_OF_TURN_RULE, [0, 0], [0, 0]);

    localEveryTurnRule.output = this.everyTurnRule;
    return localEveryTurnRule;
  }

  isSolution(newState) {
    // TODO add constraint for fire and drift
    return newState.every((e, i) =>
        this.desiredState[i] <= e)
        && this.isDriftCool(newState)
        && this.isFireCool(newState);
  }

  /**
   *
   * @param {Int8Array} newState
   * @param {number} turn
   */
  trackState(newState, turn) {
    if (!this.memoStateTurn.has(newState)) {
      //const originalTurn = this.memoStateTurn.get(newState);
      this.memoStateTurn.set(newState, turn);
    }
  }

  /**
   * StateString -> StateInstance
   *
   * @param {Int8Array} state
   * @returns {Int8Array}
   */
  createOrRetrieveStateEnum(state) {
    //console.assert(state instanceof Int8Array);
    return this.cheapStateMemo.createOrRetrieve(state);
  }

  createOrRetrieve(state) {
    const serialized = state.toString();
    if (this.memoState.has(serialized)) {
      return this.memoState.get(serialized);
    } else {
      this.memoState.set(serialized, state);
      return state;
    }
  }

  __disabled(state) {
    if (false) {
      return this.fastMemoState.createOrRetrieve(state);

    }
  }

  /**
   * State progress
   *
   * @param {Int8Array} newState
   * @param {number} turn
   * @returns {boolean}
   */
  isStateImprovement(newState, turn) {
    console.assert(newState instanceof Int8Array);
    // Note #1
    const found = this.memoStateTurn.has(newState);
    if (found) {
      // console.log('already found', newState);
    }
    return !found;
  }

  /**
   *
   * @param {Int8Array} state
   * @param {SolverRule[]} rules
   * @returns {{state:Int8Array, rule: SolverRule}[]}
   */
  calculateNextValidStates(state, rules, turn) {
    /**
     * @type {SolverRule[]}
     */
    let actualRules;
    const notEndTurn = !(rules.length === 1 && rules[0].id === -3);
    if (notEndTurn && this.limitedCommands && this.limitedCommands[turn] && this.limitedCommands[turn] !== -9) {
      //const command = this.limitedCommands[turn];
      //console.log('Should run with command', command);
      actualRules = rules.filter(r => r.id === this.limitedCommands[turn]);
    } else {
      actualRules = rules;
    }
    return actualRules.
        map(rule =>
            ({rule, newState: generateNewStateFromRule(state, rule)})).
        filter(({rule, newState}) =>
            this.areResourcesCool(newState)).
        map(({rule, newState}) =>
            ({state: this.createOrRetrieveStateEnum(newState), rule}))
        ;
  }

  /**
   *
   * @param {Int8Array} updatedState
   * @param {number} taskIndex
   * @returns {boolean}
   */
  isValidRule(updatedState, taskIndex) {
    const valid = this.areResourcesCool(updatedState)
        && this.isElectricityCool(updatedState)
        && this.isFireCool(updatedState)
        && this.isRadiationCool(updatedState);
    if (!valid) {
      return false;
    }
    return this.canStillReachDesiredState(updatedState, taskIndex);
  }

  /**
   *
   * @param {Int8Array} updatedState
   * @param {number} taskIndex
   * @returns {boolean}
   */
  canStillReachDesiredState(updatedState, taskIndex) {
    const remainingTasks = this.maxNumberCommands - taskIndex;
    if (!this.maxRules.has(taskIndex)) {
      // TODO decrease by current number of
      // const endTurnSeen = Math.floor(taskIndex / this.commands);
      const coeff = Math.ceil(remainingTasks * (1 - 1 / this.commands));
      // console.assert(coeff <= remainingTasks);
      const rule = this.ruleMaximumIntensity.map(e => e * coeff);
      this.maxRules.set(taskIndex, rule);
    }
    const rule = this.maxRules.get(taskIndex);

    const canMakeSingleResources = this.ruleIndexes.every(index =>
        (updatedState[index] + rule[index] - this.desiredState[index]) >= 0);

    if (canMakeSingleResources && this.checkCanMakeAllResources) {
      const allRemainingResources = this.ruleIndexes.reduce((acc, index) =>
          acc + this.desiredState[index] - updatedState[index], 0);
      const canMakeAllResources = (this.ruleMaximumResources * remainingTasks) > allRemainingResources;
      return canMakeAllResources;
    }


    return canMakeSingleResources;
  }

  /**
   *
   * @param turn
   * @returns {boolean}
   */
  isEndTurn(turn) {
    return (turn + 1) % (this.commands + 1) === 0;
  }

  /**
   *
   * @param {Int8Array} updatedState
   * @returns {boolean}
   */
  areResourcesCool(updatedState) {
    // console.log(updatedState);
    return updatedState.every((e, i) =>
        e >= 0
        || this.excludedFromNegativeCheck.indexOf(i) !== -1);
  }

  /**
   *
   * @param {Int8Array} newState
   */
  isFireCool(newState) {
    const fireValue = newState[fireIndex];
    return (fireValue >= this.fireLowerLimit) &&
        (fireValue <= this.fireUpperLimit);
  }

  /**
   *
   * @param {Int8Array} newState
   * @returns {boolean}
   */
  isElectricityCool(newState) {
    const electricity = newState[electricityIndex];
    return (electricity >= this.minimumElectricity)
        && (electricity <= this.maximumElectricity);
  }

  isRadiationCool(newState) {
    return newState[radiationIndex] === 0;
  }

  isDriftCool(updatedState) {
    const drift = updatedState[driftIndex];
    const isDriftOk = (drift >= this.minimumDrift) &&
        (drift <= this.maximumDrift);
    return isDriftOk;
  }

  /**
   *
   * @param {function(turn: number, states: Explorer[])} progressTrackingFunction
   */
  setProgressTracker(progressTrackingFunction) {
    this.progressTrackingFunction = progressTrackingFunction;
  }
}

export default XxlSolver;
