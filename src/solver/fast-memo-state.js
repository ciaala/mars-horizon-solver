import {NumberOfResources} from '../model/ResourceMap';

export class FastMemoState {
  /**
   * @type {Map.<number,Map>}
   */
  root;

  constructor() {
    this.root = new Map();
    this.depth = NumberOfResources;
  }

  /**
   *
   * @param {Int8Array} state
   */
  createOrRetrieve(state) {
    let index = 0;
    let current = this.root;
    while (index < this.depth) {
      // the resource counter for that index-resource
      const key = state[index];
      const isPresent = current.has(key);
      if (!isPresent) {
        return this._create(state, current, index);
      }
      current = current.get(key);
      index++;
    }
    /**
     *
     * @type {Int8Array}
     */
    const originalState = current;
    return originalState;
  }

  /**
   *
   * @param {Int8Array} state
   * @param {Map<number, Map>} current
   * @param {number} index
   * @returns {Int8Array} the same state
   */
  _create(state, current, index) {
    while (index < this.depth - 1) {
      const next = new Map();
      current.set(state[index], next);
      current = next;
      index++;
    }
    /**
     *
     * @type {Map<number, Int8Array>}
     */
    const last = current;
    last.set(state[index], state);
    return state;
  }
}
