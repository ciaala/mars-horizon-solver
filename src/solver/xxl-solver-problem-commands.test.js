import SolverSolutionTransformer from '../model/SolverSolutionTransformer';
import XxlSolver from './xxl-solver';

const data = {
  "productionRules": [
    {
      "id": 0,
      "input": [
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ],
      "output": [
        0,
        3,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 1,
      "input": [
        0,
        0,
        1,
        2,
        0,
        1,
        0,
        0,
        0
      ],
      "output": [
        0,
        5,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 2,
      "input": [
        0,
        0,
        3,
        0,
        0,
        5,
        0,
        0,
        0
      ],
      "output": [
        0,
        6,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 3,
      "input": [
        1,
        0,
        0,
        0,
        0,
        1,
        0,
        0,
        0
      ],
      "output": [
        0,
        0,
        5,
        0,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 4,
      "input": [
        0,
        3,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ],
      "output": [
        0,
        0,
        6,
        2,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 5,
      "input": [
        2,
        0,
        0,
        2,
        0,
        0,
        0,
        0,
        0
      ],
      "output": [
        0,
        0,
        10,
        0,
        0,
        0,
        0,
        0,
        0
      ]
    },
    {
      "id": 6,
      "input": [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ],
      "output": [
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ]
    }
  ],
  "initialState": [
    12,
    0,
    0,
    0,
    0,
    5,
    0,
    0,
    0
  ],
  "desiredState": [
    0,
    25,
    25,
    0,
    0,
    0,
    0,
    0,
    0
  ],
  "everyTurn": [
    0,
    0,
    0,
    0,
    0,
    5,
    0,
    0,
    0
  ],
  "turns": 4,
  "commands": 4,
  "config": {
    "heat": {
      "minimum": 0,
      "maximum": 4
    },
    "drift": {
      "minimum": -1,
      "maximum": 1,
      "isReset": false
    },
    "electricity": {
      "minimum": 1
    }
  }
};

describe('cannot do this computation', () => {
  it('abc', () => {
    const solver = new XxlSolver(data.productionRules,
        data.initialState,
        data.desiredState,
        data.everyTurn,
        data.turns,
        data.commands,
        data.config,
    );

    const solutions = solver.run()
    console.log({solutions});
    const solution = SolverSolutionTransformer.findBestSolution(solutions);
    console.log({solution});
    const transform = SolverSolutionTransformer.transform(solution, data.commands);
    console.log({transform});
  });
})
