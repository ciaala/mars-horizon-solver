import XxlSolver from './xxl-solver';

const data = {
  'productionRules': [
    {
      'id': 0,
      'input': [
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
      ],
      'output': [
        0,
        2,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
      ],
    },
    {
      'id': 1,
      'input': [
        0,
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
      ],
      'output': [
        0,
        0,
        2,
        0,
        0,
        0,
        0,
        0,
        0,
      ],
    },
    {
      'id': 2,
      'input': [
        0,
        0,
        1,
        0,
        0,
        0,
        0,
        0,
        0,
      ],
      'output': [
        0,
        0,
        0,
        2,
        0,
        0,
        0,
        0,
        0,
      ],
    },
    {
      'id': 3,
      'input': [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
      ],
      'output': [
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
      ],
    },
  ],
  'initialState': [
    4,
    0,
    0,
    0,
    1,
    0,
    0,
    0,
    0,
  ],
  'desiredState': [
    1,
    5,
    5,
    5,
    0,
    0,
    0,
    0,
    0,
  ],
  'everyTurnRule': [
    0,
    0,
    0,
    0,
    1,
    0,
    0,
    0,
    0,
  ],
  'turns': 5,
  'commands': 3,
  'options': {
    'drift': {
      'minimum': -1,
      'maximum': 1,
      'isReset': false,
    },
    'heat': {
      'minimum': 0,
      'maximum': 3,
    },
    'electricity': {
      'minimum': 1,
    },
  },
};
describe('some', () => {
  it('eex', () => {
    //data.turns = 6;
    const solver = new XxlSolver(data.productionRules,
        data.initialState,
        data.desiredState,
        data.everyTurnRule,
        data.turns,
        data.commands,
        data.options,
    );
    solver.setProgressTracker(
        (turn, states) => {
          const groups = [];
          states.forEach(e => {
            const sum = e.state.reduce((sum, e) => sum + e, 0);
            if (!groups[sum]) {
              groups[sum] = [];
            }
            groups[sum].push(e.state);
          });
          const asText = groups.map(g => g.map(s => s.toString() + '\n') + '\n');
          console.log(
              {
                turn,
                groups: asText,
              });
        });
    const solutions = solver.run();
    console.log(solutions);
  });
});
