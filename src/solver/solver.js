import {getResourceIndex, ResourceIds} from '../model/ResourceMap';

const LOWEST_SCORE = -Math.pow(2, 16);

/**
 * @typedef {Object} SolverRule
 * @property {number} id
 * @property {number[]} input
 * @property {number[]} output
 */

/**
 * @typedef {Object} TurnCommands}
 * @property {number[]} newState
 * @property {number[]} commands
 */

/**
 *
 * @typedef {object} Best
 * @property {number[]} newState
 * @property {number[]} commands
 * @property {number} move
 * @property {number} score
 */


/**
 *
 * @param {Int8Array} resourceState
 * @returns {function(acc: number,value: number,index: number): number}
 */
function scoreStateFunction(resourceState) {
    const fireIndex = getResourceIndex(ResourceIds.FIRE);
    const driftIndex = getResourceIndex(ResourceIds.DRIFT);
    const radiationIndex = getResourceIndex(ResourceIds.RADIATION);

    // Value are from the requirements
    return (acc, value, index) => {

        if (index !== fireIndex && index !== driftIndex && index !== radiationIndex) {
            return acc + (value <= resourceState[index] ? 0 : resourceState[index] - value);
        } else {
            const diff = Math.abs(resourceState[index]);
            return acc - (diff > 4 ? 100 + diff : diff);
        }
    };
}

/**
 *
 * @param {number[]} resourceState
 * @param {number[]} newResourceState
 * @param {number[]} requirements
 * @returns {number}
 */
function scoreResourcesInNewState(resourceState, newResourceState, requirements) {
    // how far is the vector get worst with the square
    // the bigger the difference the better
    // small changes count less

    // how far was the old state compared to the new one
    // more is better

    /*
     const diff = requirements.reduce(

         (acc, value, index) => acc
             + Math.abs(value - resourceState[index])
             - Math.abs(value - newResourceState[index]), 0);
     */

    //
    // values from 0 to -oo
    // if above requirements > 0 then it is 0
    //
    const howMuchWas = requirements.reduce(

        scoreStateFunction(resourceState), 0);

    //
    // values from 0 to -oo
    // if above requirements > 0 then it is 0
    //
    const howMuchRemain = requirements.reduce(
        scoreStateFunction(newResourceState), 0);
    //
    //
    //
    // if (!isDriftOk(newResourceState)) {
    //     howMuchRemain -= LOWEST_SCORE / 2;
    // }
    //
    // if (!isDriftOk(resourceState)) {
    //     howMuchWas -= LOWEST_SCORE / 2;
    // }

    // if no improvements
    if (howMuchWas <= howMuchRemain) {
        return howMuchRemain;
    } else {
        return LOWEST_SCORE;
    }
}

/**
 *
 * @param {number[]} state
 * @param {SolverRule} rule
 * @returns {number[]}
 */
function updateStateWithProductionRule(state, rule) {
    return state.map((e, index) =>
        e + rule.output[index] - rule.input[index]
    );
}

let moveAnalyzed = 0;

/**
 * @param {number[]} expl sequence of commands
 * @param {number} max max value for a command
 * @param {number} cmds maximum number of commands
 */
function nextCommandSet(expl, max, cmds) {
    if ((expl.length === cmds) && (expl[cmds - 1] < max)) {
        expl[cmds - 1]++;

        moveAnalyzed++;
        return true;
    }

    let value = expl.pop();
    while (expl.length > 0 && value >= max) {
        value = expl.pop();
    }
    if (value === max && expl.length === 0) {
        //console.error('Unable to calculate the next move from ' + expl);
        moveAnalyzed++;
        return undefined;
    } else {
        expl.push(value + 1);
        while (expl.length < cmds) {
            expl.push(0);
        }
        moveAnalyzed++;
        return true;
    }
}

/**
 * @param {number[]} newState
 */
function isDriftOk(newState) {
    const driftIndex = getResourceIndex(ResourceIds.DRIFT);
    const predicate = Math.abs(newState[driftIndex]) <= 1;
    return predicate;
}

/**
 * @param {number[]} state
 * @param {number[]} commands
 * @param {number} score
 * @param {Array.<number[]>} productionRules
 */
function printTurn(state, commands, score, productionRules) {
    console.log({score, state});
    for (let command of commands) {
        const rule = productionRules[command];
        console.log('--', JSON.stringify({command, rule}));
    }
}

/**
 * Are all the resources to be consumed available ?
 * @param {number[]} _state
 * @param {number[]} _consumables
 * @returns {boolean}
 */
function canConsume(_state, _consumables) {
    for (let k = 0; k < _consumables.length; k++) {
        if (_state[k] < _consumables[k]) {
            return false;
        }
    }
    return true;
}

class BruteForceSolver {
    /**
     * @type {Array}
     */
    productionRules;
    initialState;
    desiredState;
    /**
     * @type {number[]}
     */
    everyTurnRule;
    turns;
    commands;
    minimumElectricity;
    totalMoves;

    /**
     *
     * @param {SolverRule} rule
     */
    sumResources(rule) {
        if (this.sumResourcesMemo[rule.id]) {
            return this.sumResourcesMemo[rule.id];
        }
        const value = rule.output.reduce((acc, e) => e + acc) -
            rule.input.reduce((acc, e) => e + acc);
        this.sumResourcesMemo[rule.id] = value;
        return value;
    }

    sumResourcesMemo = {};

    constructor(productionRules, initialState, desiredState, everyTurnRule, turns, commands) {
        //.sort((a, b) => this.sumResources(a) - this.sumResources(b));
        this.productionRules = productionRules;
        this.initialState = initialState;
        this.desiredState = desiredState;
        this.everyTurnRule = everyTurnRule;
        this.turns = turns;
        this.commands = commands;
        this.minimumElectricity = Math.ceil(this.commands / 2);
        this.totalMoves = Math.pow(this.productionRules.length, this.commands);
        console.log({productionRules, initialState, desiredState, everyTurnRule, turns, commands});
    }

    /**
     *
     * @returns {TurnCommands[]}
     */
    run() {
        moveAnalyzed = 0;
        const turnSolutions = [];

        let currentState = this.initialState.slice();

        for (let turn = 1; turn <= this.turns; turn++) {
            console.log('Turn ' + turn);
            let bestScore = LOWEST_SCORE;
            let commandExplorer = new Array(this.commands).fill(this.productionRules.length - 1);
            commandExplorer[0] = -1;
            let notFinish = true;
            /**
             *
             * @type {Best}
             */
            let best = null;

            let move = 0;
            while (notFinish && move < this.totalMoves) {

                const result = this.generateTurnCommands(currentState.slice(), commandExplorer);
                //console.log({move, result});
                if (result !== undefined) {
                    const score = scoreResourcesInNewState(currentState,
                        result.newState, this.desiredState);
                    //console.log({score});
                    if (score > bestScore) {
                        // TODO
                        // && isDriftOk(result.newState)
                        best = {score, ...result, move};
                        bestScore = score;
                        console.log(JSON.stringify({best, move}));
                    }

                } else {
                    notFinish = false;
                }
                move++;
            }
            if (!best) {
                console.error('Could not generate resource plan at turn ' + turn, {moveAnalyzed});
                return [];
            }
            if (turn !== this.turns) {
                best.newState = this.generateEndOfTurnResource(best.newState);
            }
            currentState = best.newState;
            printTurn(currentState, best.commands, best.score, this.productionRules);
            turnSolutions.push(best);

            if (best.score === 0) {
                console.error("Found BEST at 000");
                break;
            }
        }
        console.log({moveAnalyzed});
        return turnSolutions;
    }

    astronautIndex = getResourceIndex(ResourceIds.ASTRONAUT);

    /**
     *
     * @param {} currentState
     * @returns {}
     */
    generateEndOfTurnResource(currentState) {
        if (false) {
            /**
             *
             * @type {number[]}
             */
            const tempInput = new Array(this.everyTurnRule.length).fill(0);
            /**
             *
             * @type {SolverRule}
             */
            const rule = {id: 0, input: tempInput, output: this.everyTurnRule};
            const result = updateStateWithProductionRule(currentState, rule);
        }
        //  set astronaut to the expected number
        const result = currentState.map((e, i) => i !== this.astronautIndex ? e + this.everyTurnRule[i] : this.everyTurnRule[i]);
        //.map(e => e > 0 ? e : 0);
        // console.log('new state after end turn', {result, currentState, rule: this.everyTurnRule});
        return result;
    }


    /**
     * return the selected commands, the newState
     * @param {number[]} state
     * @param {number[]} commandExplorer
     * @return {TurnCommands}
     */
    generateTurnCommands(state, commandExplorer) {

        let found = true;

        const commandIndexLimit = this.productionRules.length - 1;

        while (found !== undefined) {
            found = nextCommandSet(commandExplorer, commandIndexLimit, this.commands);
            if (found) {
                const {found, newState} = this.canRunSequenceOfCommands(commandExplorer, state.slice());
                if (found) {
                    return {
                        newState,
                        'commands': commandExplorer.slice(),
                    };
                }
            }
        }
        return undefined;
    }

    /**
     *
     * @type {{found: boolean, newState: []}}
     */
    static
    NOT_FOUND = {found: false, newState: []};


    excluded = [getResourceIndex(ResourceIds.DRIFT)];

    /**
     *
     * @param {number[]} commandExplorer
     * @param {number[]} originalState
     * @returns {{boolean, number[] }} {found,number[]}
     */
    canRunSequenceOfCommands(commandExplorer, originalState) {

        let clonedState = originalState.slice();

        for (let i = 0; i < this.commands; i++) {

            const iCommand = commandExplorer[i];

            const p = this.productionRules[iCommand];

            if (!canConsume(clonedState, p.input)) {
                return BruteForceSolver.NOT_FOUND;
            }
            const updatedState = updateStateWithProductionRule(clonedState, p);
            const canRunCommand =
                this.areResourcesCool(updatedState)
                && this.isElectricityCool(updatedState, clonedState)
                && this.isFireCool(updatedState, clonedState)
                && this.isRadiationCool(updatedState, clonedState);

            if (!canRunCommand) {
                return BruteForceSolver.NOT_FOUND;
            }
            clonedState = updatedState;
        }
        return {found: true, newState: clonedState};
    }

    areResourcesCool(updatedState) {
        return updatedState.every(e => e >= 0 || this.excluded.indexOf(e) !== -1);
    }

    /**
     *
     * @param {number[]} newState
     * @param {number[]} originalState
     */
    isFireCool(newState, originalState) {
        const fireIndex = getResourceIndex(ResourceIds.FIRE);
        return newState[fireIndex] <= 5;
    }

    /**
     *
     * @param {number[]} newState
     * @param {number[]} originalState
     * @returns {boolean}
     */
    isElectricityCool(newState, originalState) {
        const electricityIndex = getResourceIndex(ResourceIds.ELECTRICITY);
        const isHigh = newState[electricityIndex] >= this.minimumElectricity;
        return isHigh || (newState[electricityIndex] >= originalState[electricityIndex]);
    }


    isRadiationCool(newState, originalState) {
        const radiationIndex = getResourceIndex(ResourceIds.RADIATION);
        return newState[radiationIndex] === 0;
    }
}

/**
 *
 * @param table
 * @param turns
 * @param commands
 * @returns {turnSolutions: []}
 */
function

findSolution(productionRules, initialState, desiredState, everyTurnRule, turns, commands) {

    //return runBruteForceSolution(productionRules, [turns, commands], initialState, desiredState, everyTurns);
    const solver = new BruteForceSolver(productionRules, initialState, desiredState, everyTurnRule, turns, commands);
    const solution = solver.run();
    console.log(solution);
    return solution;
}

export {
    nextCommandSet,
    findSolution,
    scoreResourcesInNewState,
    scoreStateFunction,
    LOWEST_SCORE,
    updateStateWithProductionRule,
};
export default BruteForceSolver;
