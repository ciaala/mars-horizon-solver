# Known Issues

## Does not calculate correctly the previous state [Solved 2020-12-19]

it is not possible to create the tree of all the states with a single map.

Each node has could be visited coming from multiple other parent nodes.

Better to calculate the tree once the solution has been found.

Keeping track of state tree is not worth because it keeps a lot of memory in use whie calculating few cases later would be fast.

## Drift does not have a range check that allows negative values [Solved 2020-12-19]


