import {NumberOfResources} from '../model/ResourceMap';
import XxlSolver from './xxl-solver';
import {
  astronautIndex,
  dataIndex, driftIndex, electricityIndex, fireIndex, makeI8State,
  makeRule,
  makeState, radiationIndex,
  rocketIndex,
} from './utility-solver.test';
import _ from 'lodash';

/**
 * +1
 * @type {{output: number[], input: number[], id: number}}
 */
const electricityRule = makeRule(0, [0, 0], [electricityIndex, 1]);
const rocketRule = makeRule(21, [dataIndex, 2], [rocketIndex, 1]);
const radiationRule = makeRule(22, [radiationIndex, 1], [dataIndex, 1]);

/**
 * ..-1..1
 * @type {{output: number[], input: number[], id: number}}
 */
const fireRule = makeRule(23, [fireIndex, 1], [dataIndex, 1]);
/**
 * -2...1
 * @type {{output: number[], input: number[], id: number}}
 */
const slowDataRule = makeRule(1, [electricityIndex, 2], [dataIndex, 1]);

const dataRule = makeRule(2, [electricityIndex, 3], [dataIndex, 2]);
const fastDataRule = makeRule(3, [electricityIndex, 4], [dataIndex, 3]);

function toI8A(original, size = undefined) {
  if (size) {
    const result = new Int8Array(size);
    result.set(original);
    return result;
  } else {
    return new Int8Array(original);
  }
}

const END_OF_TURN_RULE = -3;

describe('Test the new xxl-solver', () => {
  const rules = [
    // 0
    electricityRule,
    // 1
    slowDataRule,
    dataRule,
    fastDataRule,
    // 21
    rocketRule,
  ];
  // GIVEN
  // #10 energy
  const initialState = makeState(electricityIndex, 10);
  // REQUIRED
  // #4 data
  const outputState = makeState(dataIndex, 4);
  // AT EACH TURN
  // NO CHANGE
  const everyTurn = makeState(electricityIndex, 0);

  const defaultOptions = {
    drift: {minimum: -1, maximum: 1, isReset: false},
    heat: {minimum: 0, maximum: 3},
    electricity: {minimum: 2},
  };

  it('the new solver can be instantiated', () => {

    const solver = new XxlSolver(
        rules,
        initialState,
        outputState,
        everyTurn,
        1, 4,
        defaultOptions);
    const solutions = solver.run();
    const final3State = makeState(dataIndex, 3);
    const final4State = makeState(dataIndex, 4);
    const final5State = makeState(dataIndex, 5);
    const final6State = makeState(dataIndex, 6);

    final3State[electricityIndex] = 6;
    final4State[electricityIndex] = 4;
    final5State[electricityIndex] = 3;
    final6State[electricityIndex] = 2;
    const expected = [
      {
        states: [toI8A(final4State), toI8A(final3State), toI8A(initialState)],
        move: 2,
        commands: toI8A([fastDataRule.id, slowDataRule.id]),
      },
      {
        states: [toI8A(final5State), toI8A(final3State), toI8A(initialState)],
        move: 2,
        commands: toI8A([fastDataRule.id, dataRule.id]),
      },
      {
        states: [toI8A(final6State), toI8A(final3State), toI8A(initialState)],
        move: 2,
        commands: toI8A([fastDataRule.id, fastDataRule.id]),
      },
    ];
    expect(solutions).toHaveLength(3);
    expect(solutions[0]).toStrictEqual(expected[0]);
    expect(solutions[1]).toStrictEqual(expected[1]);
    expect(solutions[2]).toStrictEqual(expected[2]);
  });

  it('can solve on multiple turns', () => {
    const rules = [
      // 1
      slowDataRule,
    ];
    const solver = new XxlSolver(
        rules,
        initialState,
        outputState,
        everyTurn,
        4, 3,
        defaultOptions,
    );
    const solutions = solver.run();

    const expected = [
      {
        states: [
          makeI8State(2, 4),
          makeI8State(4, 3),
          makeI8State(4, 3),
          makeI8State(6, 2),
          makeI8State(8, 1),
          toI8A(initialState),
        ],
        move: 5,
        commands: toI8A(
            [
              slowDataRule.id,
              slowDataRule.id,
              slowDataRule.id,
              END_OF_TURN_RULE,
              slowDataRule.id,
            ],
        ),
      },

    ];

    expect(solutions).toStrictEqual(expected);
  });

  it('can solve multiple rules in multiple turns',
      () => {
        const rules = [
          // 1
          slowDataRule,
          //  dataRule,
          fireRule,
          rocketRule,
          electricityRule,
        ];
        const _outputState = outputState.slice();
        _outputState[dataIndex] = 5;
        const _everyTurn = everyTurn.slice();
        _everyTurn[fireIndex] = 2;
        const solver = new XxlSolver(
            rules,
            initialState,
            _outputState,
            _everyTurn,
            4, 2,
            defaultOptions);

        const solutions = solver.run();

        const solution0 = {
          states: [
            makeI8State(2, 5, 3),

            makeI8State(4, 4, 3),
            makeI8State(4, 4, 1),
            makeI8State(6, 3, 1),

            makeI8State(6, 2, 2),
            makeI8State(6, 2),
            makeI8State(8, 1),

            toI8A(initialState)],
          move: 28,
          commands: toI8A(
              [
                slowDataRule.id, // 1
                slowDataRule.id, // 2
                END_OF_TURN_RULE,
                fireRule.id, // 3
                slowDataRule.id, // 4
                END_OF_TURN_RULE,
                slowDataRule.id, // 5
              ]),
        };

        const solution1 = {
          states: [
            makeI8State(4, 5, 2),

            makeI8State(4, 4, 3),
            makeI8State(4, 4, 1),
            makeI8State(6, 3, 1),

            makeI8State(6, 2, 2),
            makeI8State(6, 2),
            makeI8State(8, 1),

            toI8A(initialState)],
          move: 28,
          commands: toI8A(
              [
                slowDataRule.id, // 1
                slowDataRule.id, // 2
                END_OF_TURN_RULE,
                fireRule.id, // 3
                slowDataRule.id, // 4
                END_OF_TURN_RULE,
                fireRule.id, // 5
              ]),
        };
        // This state is doing one more transformation from Fire to Data
        const solution2 = _.cloneDeep(solution1);
        solution2.commands.fill(fireRule.id, 4, 5);
        solution2.move = 29;
        solution2.states[0] = makeI8State(6, 5, 1);
        solution2.states[1] = makeI8State(6, 4, 2);
        solution2.states[2] = makeI8State(6, 4, 0);

        //solution2.states[0] = makeI8State()
        const solution3 = _.cloneDeep(solution0);

        solution3.commands.fill(slowDataRule.id, 3, 4);
        solution3.move = 31;
        solution3.states[0] = makeI8State(0, 5, 4);
        solution3.states[1] = makeI8State(2, 4, 4);
        solution3.states[2] = makeI8State(2, 4, 2);
        solution3.states[3] = makeI8State(4, 3, 2);
        const expected = [
          solution0,
          solution1,
          solution2,
          solution3,
        ];
        expect(solutions.length).toBe(4);
        // Solution should have 5 data
        expect(solutions[0]).toStrictEqual(expected[0]);
        expect(solutions[1]).toStrictEqual(expected[1]);
        expect(solutions[2]).toStrictEqual(expected[2]);

        expect(solutions[3]).toStrictEqual(expected[3]);
        expect(solutions).toStrictEqual(expected);
      });
  it('The solver keep the radiation to 0 at the end of each turn',
      () => {

        const rules = [
          // 0
          electricityRule,
          // 1
          slowDataRule,
          // 22
          radiationRule,
        ];
        const _outputState = outputState.slice();
        _outputState[dataIndex] = 2 + 2 + 2;
        const _everyTurn = everyTurn.slice();
        _everyTurn[radiationIndex] = 1;
        const _initialState = initialState.slice();
        _initialState[electricityIndex] = 6;
        const solver = new XxlSolver(
            rules,
            _initialState,
            _outputState,
            _everyTurn,
            4, 2,
            defaultOptions);
        const solution = solver.run();
        expect(solution.length).toStrictEqual(1);

        //const final6State = makeState(dataIndex, 6);
        expect(solution).toStrictEqual([
          {
            states: [
              makeI8State(1, 6, 0, 0),

              makeI8State(1, 5, 0, 1),
              makeI8State(1, 5, 0, 0),
              makeI8State(3, 4, 0, 0),

              makeI8State(3, 3, 0, 1),
              makeI8State(3, 3, 0, 0),
              makeI8State(2, 3, 0, 0),

              makeI8State(2, 2, 0, 1),
              makeI8State(2, 2, 0, 0),
              makeI8State(4, 1, 0, 0),

              makeI8State(6, 0, 0),

            ],
            move: 29,
            commands: toI8A([
              slowDataRule.id,
              slowDataRule.id,
              END_OF_TURN_RULE,
              radiationRule.id,
              electricityRule.id,
              END_OF_TURN_RULE,
              radiationRule.id,
              slowDataRule.id,
              END_OF_TURN_RULE,
              radiationRule.id,
            ]),
          },
        ]);
      });

  it('Accepts and solve with negative drift ', () => {
    const _initialState = initialState.slice();
    _initialState[driftIndex] = -2;
    const _rules = rules.slice();
    _rules.push(makeRule(99, [electricityIndex, 1], [driftIndex, 1]));
    const solver = new XxlSolver(
        _rules,
        _initialState,
        outputState,
        everyTurn,
        2, 4,
        defaultOptions);

    const solution = solver.run();
    expect(solution).toBeDefined();
    expect(solution).toHaveLength(3);

  });
  it('Sub-optimal path are pruned if they cannot produce fast enough', () => {
    const _rules = rules.slice();
    _rules.push(makeRule(99, [electricityIndex, 0], [astronautIndex, 4]));
    outputState[dataIndex] = 30;
    const solver = new XxlSolver(
        _rules,
        initialState,
        outputState,
        everyTurn,
        12, 4,
        defaultOptions);
    const maximumRule = new Array(NumberOfResources).fill(0);
    maximumRule[electricityIndex] = 1;
    maximumRule[dataIndex] = 3;
    maximumRule[astronautIndex] = 4;
    maximumRule[rocketIndex] = 1;
    expect(solver.ruleMaximumIntensity).toStrictEqual(maximumRule);
    expect(solver.ruleIndexes).toStrictEqual([dataIndex]);
    let prunedEveryAstronaut;

    solver.setProgressTracker((turn, workQueue) => {
      if (turn && (turn % 4 === 0)) {
        const ai = workQueue.map(e => e.state[astronautIndex]);
        prunedEveryAstronaut = workQueue.every(
            e => e.state[astronautIndex] < 3 * 4);
      }
    });

    const solution = solver.run();
    expect(solution).toBeDefined();
    expect(prunedEveryAstronaut).toBeDefined();
    expect(prunedEveryAstronaut).toBe(true);
  });
});

const grandTourVoyager = {
  'productionRules': [
    {
      'id': 0,
      'input': [2, 0, 0, 0, 0, 0, 0, 0, 1],
      'output': [0, 3, 0, 0, 0, 0, 0, 0, 0],
    },
    {
      'id': 1,
      'input': [0, 1, 1, 0, 0, 0, 0, 0, 2],
      'output': [0, 5, 0, 0, 0, 0, 0, 0, 0],
    },
    {
      'id': 2,
      'input': [0, 0, 3, 0, 0, 0, 0, 0, 0],
      'output': [0, 4, 0, 2, 0, 2, 0, 0, 0],
    },
    {
      'id': 3,
      'input': [1, 0, 0, 0, 0, 0, 0, 0, 0],
      'output': [0, 0, 0, 4, 0, 0, 0, 0, 0],
    },
    {
      'id': 4,
      'input': [1, 1, 0, 0, 0, 0, 0, 0, 0],
      'output': [0, 0, 0, 5, 0, 0, 0, 0, 0],
    },
    {
      'id': 5,
      'input': [0, 2, 0, 0, 0, 0, 0, 0, 0],
      'output': [0, 0, 2, 3, 0, 0, 0, 0, 0],
    },
    {
      'id': 6,
      'input': [1, 0, 0, 0, 0, 0, 0, 0, 0],
      'output': [0, 0, 3, 0, 0, 0, 0, 0, 0],
    },
    {
      'id': 7,
      'input': [2, 0, 0, 1, 0, 0, 0, 0, 0],
      'output': [0, 0, 6, 0, 0, 0, 0, 0, 0],
    },
    {
      'id': 8,
      'input': [0, 0, 0, 2, 0, 0, 0, 0, 0],
      'output': [0, 3, 3, 0, 0, 1, 0, 0, 0],
    },
    {
      'id': 9,
      'input': [0, 0, 0, 0, 0, 0, 0, 0, 0],
      'output': [1, 0, 0, 0, 0, 0, 0, 0, 0],
    }],
  'initialState': [9, 0, 0, 0, 0, 0, 0, 0, 4],
  'desiredState': [0, 28, 40, 28, 0, 0, 0, 0, 0],
  'everyTurnRule': [0, 0, 0, 0, 0, -3, 0, 0, 4],
  'turns': 7,
  'commands': 6,
};

function asArguments({
                       productionRules,
                       initialState,
                       desiredState,
                       everyTurnRule,
                       turns,
                       commands,
                     }) {
  return [
    productionRules,
    initialState,
    desiredState,
    everyTurnRule,
    turns,
    commands];
}

describe('testing with real model', () => {
  it('Grand Tour, saturn voyager puzzle', () => {

    const solver = new XxlSolver(...asArguments(grandTourVoyager));
    const solution = solver.run();
    expect(solution).toHaveLength(3);

  });
});
