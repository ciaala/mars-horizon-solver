import BruteForceSolver, {
    LOWEST_SCORE,
    nextCommandSet,
    scoreResourcesInNewState,
    updateStateWithProductionRule
} from './solver';
import MarsHorizonState from '../model/MarsHorizonState';
import ResourceMap, {generate, getResourceIndex, ResourceIds} from '../model/ResourceMap';
import {makeRule, makeState} from "./utility-solver.test";

const initialState = [0, 0];
const rules = [
    {id: 0, input: [1, 0], output: [0, 1]},
    {id: 1, input: [0, 0], output: [1, 0]},
];


const fireIndex = getResourceIndex(ResourceIds.FIRE);
const electricityIndex = getResourceIndex(ResourceIds.ELECTRICITY);
const dataIndex = getResourceIndex(ResourceIds.DATA);
const rocketIndex = getResourceIndex(ResourceIds.ROCKET);
const driftIndex = getResourceIndex(ResourceIds.DRIFT);


describe('test the solver', () => {
        const solver = new BruteForceSolver(
            rules,
            initialState,
            [0, 3],
            [0, 0],
            6,
            2,
        );
        it('accepts a set of a valid set of commands', () => {
            const actual = solver.canRunSequenceOfCommands([1, 1], [0, 0]);
            expect(actual).toStrictEqual({found: true, newState: [2, 0]});
        });
        it('refuses a set of a invalid commands', () => {

            expect(solver.canRunSequenceOfCommands([0, 0], [0, 0]))
                .toStrictEqual({found: false, newState: []});
            expect(solver.canRunSequenceOfCommands([0, 1], [0, 0]))
                .toStrictEqual({found: false, newState: []});
            expect(solver.canRunSequenceOfCommands([1, 0], [0, 0]))
                .toStrictEqual({found: false, newState: []});
        });

        it('finds a solution', () => {

            const solution = solver.run();
            console.log(solution);
            expect(solution.length).toBe(4);
        });
    },
);
describe('Vehicle move, do not break the calculation', () => {
    it('searching a solution with the VehicleMove does not break the solution finding', () => {

        const rules = [
            makeRule(1, [0, 0], [dataIndex, 1]),
            makeRule(1, [dataIndex, 3], [rocketIndex, 4]),
        ];

        const solver = new BruteForceSolver(
            rules,
            makeState(0, 0),
            makeState(rocketIndex, 3),
            makeState(rocketIndex, -1),
            10,
            2,
        );
        const solution = solver.run();
        expect(solution).toBeDefined();
        expect(solution).toHaveLength(0);

    });
});
describe('UpdateState is working correctly', () => {
    it('run one update', () => {
        const rule = makeRule(1, [0, 0], [driftIndex, 1]);
        const update = updateStateWithProductionRule(makeState(0, 0), rule);
        expect(update).toStrictEqual(makeState(driftIndex, 1));
    });
})
;
describe('Negative DRIFT, do not break the calculation', () => {
    it('searching a solution with the DRIFT does not break the solution finding', () => {
        const rules = [
            makeRule(1, [0, 0], [dataIndex, 1]),
            makeRule(1, [dataIndex, 3], [driftIndex, 1]),
        ];

        const solver = new BruteForceSolver(
            rules,
            makeState(driftIndex, -2),
            makeState(driftIndex, 3),
            makeState(driftIndex, -2),
            10,
            2,
        );
        const solution = solver.run();
        expect(solution).toBeDefined();
        expect(solution).toHaveLength(3);

    });
});

describe('test some algorithm', () => {
        it('scoring a result', () => {
            const score = scoreResourcesInNewState([0, 0], [1, 1], [0, 3]);
            console.log('scoring result', {score, LOWEST_SCORE});

            expect(score).toBeGreaterThan(LOWEST_SCORE);
        });
        it('enumeration of commands sequence', () => {
                const expl = [-1, 2];
                expect(nextCommandSet(expl, 1, 2)).toBeTruthy();
                expect(expl).toStrictEqual([0, 0]);

                expect(nextCommandSet(expl, 1, 2)).toBeTruthy();
                expect(expl).toStrictEqual([0, 1]);

                expect(nextCommandSet(expl, 1, 2)).toBeTruthy();
                expect(expl).toStrictEqual([1, 0]);

                expect(nextCommandSet(expl, 1, 2)).toBeTruthy();
                expect(expl).toStrictEqual([1, 1]);

                expect(nextCommandSet(expl, 1, 2)).toBeFalsy();
            },
        );

        it('makeNumeric', () => {
                const resourceKeys = Object.keys(ResourceMap);
                const numeric = MarsHorizonState.makeNumeric(generate(8), resourceKeys);
                expect(numeric).toStrictEqual(new Array(resourceKeys.length).fill(8));
            },
        );
    },
);


describe('Fire is not a problem', () => {


    it('can score fire as bad', () => {
        const initialState = makeState(fireIndex, 5);
        const state = makeState(fireIndex, 5);
        const newState = makeState(fireIndex, 3);
        const req = makeState(dataIndex, 3);

        const scoreOld = scoreResourcesInNewState(initialState, state, req);

        const scoreNew = scoreResourcesInNewState(state, newState, req);
        expect(scoreOld).toBe(-108);
        expect(scoreNew).toBe(-6);

    });
    it('can pick move to compensate for fire', () => {

        const electricityRule = makeRule(1, [0, 0], [electricityIndex, 1]);
        const dataRule = makeRule(2, [electricityIndex, 1], [dataIndex, 1]);
        const fireRule = makeRule(3, [fireIndex, 1]);

        const rules = {
            '1': electricityRule,
            '2': dataRule,
            '3': fireRule,
        };

        const resourceKeys = Object.keys(ResourceMap);
        const desiredState = MarsHorizonState.makeNumeric(generate(0), resourceKeys);
        desiredState[dataIndex] = 8;
        const everyTurnRule = MarsHorizonState.makeNumeric(generate(0), resourceKeys);
        everyTurnRule[fireIndex] = 1;
        const initialState = MarsHorizonState.makeNumeric(generate(0), resourceKeys);
        initialState[electricityIndex] = 1;
        const solver = new BruteForceSolver(
            rules,
            initialState,
            desiredState,
            everyTurnRule,
            10,
            3,
        );
        const solution = solver.run();
        expect(solution.length).toBe(9);
    });
});

