import ResourceMap, {
  getResourceIndex, ResourceIds as ResourcesIds,
  ResourceIds,
} from '../model/ResourceMap';

const numberResources = Object.keys(ResourceMap).length;

/**
 *
 * @param {number} id
 * @param {[number,number]} inputOverride
 * @param {[number,number]} outputOverride
 * @returns {{output: number[], input: number[], id: number}}
 */


const fireIndex = getResourceIndex(ResourceIds.FIRE);
const electricityIndex = getResourceIndex(ResourceIds.ELECTRICITY);
const dataIndex = getResourceIndex(ResourceIds.DATA);
const rocketIndex = getResourceIndex(ResourceIds.ROCKET);
const driftIndex = getResourceIndex(ResourceIds.DRIFT);
const radiationIndex = getResourceIndex(ResourcesIds.RADIATION);
const astronautIndex = getResourceIndex(ResourceIds.ASTRONAUT);

function makeRule(id, inputOverride = [0, 0], outputOverride = [0, 0]) {
  const rule = {
    id,
    input:
        new Array(numberResources).fill(0),
    output:
        new Array(numberResources).fill(0),
  };
  rule.input[inputOverride[0]] = inputOverride[1];
  rule.output[outputOverride[0]] = outputOverride[1];
  return rule;
}

/**
 *
 * @returns {number[]}
 */
function makeState(index, override) {
  const state = new Array(numberResources).fill(0);
  state[index] = override;
  return state;
}

/**
 *
 * @param {number} resourceIndex
 * @param {number} resourceValue
 * @returns {Int8Array}
 */
function arrayState(resourceIndex, resourceValue) {
  const state = new Int8Array(numberResources).fill(0);
  state.fill(resourceValue, resourceIndex, resourceIndex + 1);
  return state;
}

/**
 *
 * @param {number} electricity
 * @param {number} data
 * @param {number} fire
 * @param {number} radiation
 * @returns {Int8Array}
 */
function makeI8State(electricity = 0, data = 0, fire = 0, radiation = 0) {
  const state = new Int8Array(numberResources).fill(0);
  if (radiation)
    state.fill(radiation, radiationIndex, radiationIndex + 1);
  if (data)
    state.fill(data, dataIndex, dataIndex + 1);
  if (electricity)
    state.fill(electricity, electricityIndex, electricityIndex + 1);
  if (fire)
    state.fill(fire, fireIndex, fireIndex + 1);
  return state;
}

export {
  makeState, makeRule, arrayState, makeI8State,

  fireIndex,
  electricityIndex,
  dataIndex,
  rocketIndex,
  driftIndex,
  radiationIndex,
  astronautIndex
};
