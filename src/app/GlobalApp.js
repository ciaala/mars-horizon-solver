import {configureStore} from '@reduxjs/toolkit';
import * as PropTypes from 'prop-types';
import {Provider} from 'react-redux';
import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom';
import styles from '../index.module.css';
import BenchmarkCharts from '../lib/benchmark/charts';
import MarsHorizonSlice, {initialState} from '../reducer/MarsHorizonReducer';
import ManualView from '../view/ManualView';
import ExtraConfig, {FormField, FormType} from './ExtraConfig';
import MarsHorizonApp from './MarsHorizonApp';

const links = [
  {url: '/', title: 'Home'},
  {url: '/benchmarks', title: 'Benchmarks'},
  {url: '/extraConfig', title: 'Configuration'},
  {url: '/manual', title: 'Manual Plan'}
];

function NavigationBar(props) {
  const elements = props.links.map(
      link => <Link className={styles.Link} key={link.url}
                    to={link.url}>{link.title}</Link>);
  return <div className={styles.NavigationBar}>
    {elements}
  </div>;
}

NavigationBar.propTypes = {links: PropTypes.any};

const solverDefaultFields = [

  new FormField(
      FormType.NUMERIC_RANGE,
      ':fire:',
      ['/root/solver/heat/minimum', '/root/solver/heat/maximum'],
      'Heat Tolerance'),
  new FormField(
      FormType.NUMERIC_RANGE,
      ':cyclone:',
      ['/root/solver/drift/minimum', '/root/solver/drift/maximum'],
      'Drift Tolerance'),

  new FormField(FormType.BOOLEAN, ':cyclone:', '/root/solver/drift/isReset',
      'Is Drift Reset'),
  new FormField(FormType.NUMERIC, ':zap:', '/root/solver/electricity/minimum',
      'Minimum Electricity'),
];

export default function GlobalApp(props) {

  const loadFromLocalStorage = () => {
    try {
      const serializedState = localStorage.getItem('state');
      if (serializedState && serializedState.length > 0) {
        const localState = JSON.parse(serializedState);
        //console.log({localState});
        return localState;
      }
    } catch (e) {
      console.error(e);
    }
    console.log('Using initial state');
    return initialState;
  };
  const saveToLocalStorage = state => {
    try {
      const serializedState = JSON.stringify(state);
      //  console.log({serializedState});
      localStorage.setItem('state', serializedState);
    } catch (e) {
      throw new Error(e);
    }
  };
  const preloadedState = loadFromLocalStorage();

  const store = configureStore({
        reducer: MarsHorizonSlice.reducer,
        preloadedState,
      });

  store.subscribe(() => saveToLocalStorage(store.getState()));
  window.globalStore = store;

  return <Provider store={store}>
    <Router>
      <NavigationBar links={links}/>
      <Switch>
        <Route exact={true} path={'/'}>
          <MarsHorizonApp/>
        </Route>
        <Route exact={true} path={'/benchmarks'}>
          <BenchmarkCharts/>
        </Route>
        <Route exact={true} path={'/extraConfig'}>
          <ExtraConfig fields={solverDefaultFields}/>
        </Route>
        <Route exact={true} path={'/manual'}>
          <ManualView/>
        </Route>
      </Switch>

    </Router>
  </Provider>;
}
