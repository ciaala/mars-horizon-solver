import * as PropTypes from 'prop-types';
import React from 'react';
import Action from '../components/Action';
import ActionBar from '../components/ActionBar';
import styles from '../components/MarsHorizon.module.css';
import NumericValueHolder from '../components/NumericValueHolder';
import ProductionRuleTable from '../components/ProductionRuleTable';
import TableExtraRow from '../components/TableExtraRow';
import Title from '../components/Title';
import ToggleValueHolder from '../components/ToggleValueHolder';
import TurnCommands from '../components/TurnCommands';
import {
  clearAll,
  clearSolution,
  createRule,
  runSolver,
} from '../reducer/MarsHorizonReducer';
import AsRow from '../layout/AsRow';
import RequiredCommands from './RequiredCommands';

function MarsHorizonApp(props) {

  //console.log(['App', props], ['state: ', store.getState()]);
  return <div className={styles.App}>
    <Title text={'Mars Horizon App'}/>
    <AsRow>
      <NumericValueHolder title='Turns' valueHolder={'/root/turns'}
                          emoji={':cyclone:'}/>
      <NumericValueHolder title='Commands' valueHolder={'/root/commands'}
                          emoji={':cyclone:'}/>
      {/*<ToggleValueHolder title='Solver' valueHolder={'/root/solver'}*/}
      {/*                   emoji={':abacus:'}/>*/}
    </AsRow>
    <TableExtraRow title={'Initial State'} ruleId={-1} section={'output'}/>
    <TableExtraRow title={'Output'} ruleId={-2} section={'output'}/>
    <TableExtraRow title={'End Of Turn'} ruleId={-3} section={'output'}/>
    <ProductionRuleTable>
      <ActionBar>
        <Action onClick={createRule} emojiIdentifier={':heavy_plus_sign:'}/>
        <Action onClick={runSolver} emojiIdentifier={':computer:'}/>
        <Action onClick={clearAll} emojiIdentifier={':firecracker:'}
                shouldConfirm={true} confirmMessage={'Erase Table'}/>
        <Action onClick={clearSolution} emojiIdentifier={':lotion_bottle:'}
                shouldConfirm={true} confirmMessage={'Erase Solution'}/>
      </ActionBar>
    </ProductionRuleTable>
    <RequiredCommands selector={'/root/requiredCommands'}/>
    <TurnCommands selector={'/root/solution'}/>
  </div>;
}

export default MarsHorizonApp;
