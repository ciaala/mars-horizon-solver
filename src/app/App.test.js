import { render, screen } from '@testing-library/react';
import MarsHorizonApp from './App';

test('renders learn react link', () => {
  render(<MarsHorizonApp />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
