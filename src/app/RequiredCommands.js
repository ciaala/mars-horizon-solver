import * as PropTypes from 'prop-types';
import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {setValueHolder} from '../reducer/MarsHorizonReducer';
import styles from '../components/MarsHorizon.module.css';

function textChangeHandler(dispatch, valueHolder) {
  return event => {
    const value = event.target.value;
    const payload = {valueHolder, value};
    const action = setValueHolder(payload);
    dispatch(action);
  };
}

function RequiredCommands(props) {
  const dispatch = useDispatch();
  const requiredCommands = useSelector(
      state => state.valueHolders[props.selector]);

  return <textarea className={styles.RequiredCommands} onChange={textChangeHandler(dispatch,
      props.selector)} value={requiredCommands}/>;
}

RequiredCommands.propTypes = {
  selector: PropTypes.string,
};

export default RequiredCommands;
