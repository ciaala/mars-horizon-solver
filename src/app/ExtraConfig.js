import * as PropTypes from 'prop-types';
import MinMaxRange from '../components/MinMaxRange';
import NumericValueHolder from '../components/NumericValueHolder';
import ToggleValueHolder from '../components/ToggleValueHolder';
import styles from './ExtraConfig.module.css';

export const FormType = {
  NUMERIC: 'numeric',
  BOOLEAN: 'boolean',
  NUMERIC_RANGE: 'numericRange',
};

function createFormElement({type, emoji, selector, title}) {
  if (type === FormType.NUMERIC_RANGE) {
    return <MinMaxRange key={title}
                        minValueHolder={selector[0]}
                        maxValueHolder={selector[1]}
                        emoji={emoji}
                        title={title}/>;
  } else if (type === FormType.NUMERIC) {
    return <NumericValueHolder key={title}
                               valueHolder={selector} emoji={emoji}
                               title={title}/>;
  } else if (type === FormType.BOOLEAN) {
    return <ToggleValueHolder key={title}
                              valueHolder={selector} emoji={emoji}
                              title={title}/>;
  }
  console.error('Unknown Type', type);
  return <span>ERROR !!!</span>;
}

export class FormField {
  type;
  emoji;
  selector;
  title;

  /**
   *
   * @param {string} type
   * @param {string} emoji
   * @param {string|Array.<string>} selector
   * @param {string} title
   */
  constructor(type, emoji, selector, title) {
    this.type = type;
    this.emoji = emoji;
    this.selector = selector;
    this.title = title;

  }
}

function ExtraConfig(props) {
  /**
   *
   * @type {FormField[]}
   */
  const fields = props.fields;
  const formElements = fields.map(field => createFormElement(field));
  return <div className={styles.ExtraConfig}>{formElements}</div>;
}

ExtraConfig.propTypes = {
  fields: PropTypes.arrayOf(PropTypes.instanceOf(FormField)),
};

export default ExtraConfig;
