import * as PropTypes from 'prop-types';
import React from 'react';
import styles from '../components/MarsHorizon.module.css';

function AsRow(props) {
  return <div className={styles.AsRow}>{props.children}</div>;
}

AsRow.propTypes = {children: PropTypes.node};
export default AsRow;
