import {
  astronautIndex,
  dataIndex,
  electricityIndex, makeI8State,
  makeRule, makeState,
} from '../solver/utility-solver.test';

import XxlSolver from '../solver/xxl-solver';
import SolverSolutionTransformer from './SolverSolutionTransformer';

function createSolverSolution(numberOfCommands) {
  console.assert(numberOfCommands);
  let productionRules = [
    makeRule(0, [electricityIndex, 0], [electricityIndex, 1]),
    makeRule(1, [electricityIndex, 1], [dataIndex, 1]),
  ];
  const initialState = makeState(0, 0);
  const everyTurnRule = makeState(0, 0);
  const desiredState = makeState(dataIndex, 4);

  const xxlSolver = new XxlSolver(productionRules,
      initialState,
      desiredState,
      everyTurnRule,
      10,
      numberOfCommands,
  );
  return xxlSolver.run();
}

function makeA8State(electricity, data) {
  return Array.from(makeI8State(electricity, data));
}

describe('SolutionSolveTransformer', () => {
  it('Accepts a solver solution and prepare a visual model', () => {
        let numberOfCommands = 3;

        const solution = createSolverSolution(numberOfCommands);
        expect(solution).toBeDefined();
        expect(solution).toHaveLength(1);

        const actual = SolverSolutionTransformer.transform(
            solution[0],
            numberOfCommands,
            makeState(0, 0));
        expect(actual).toBeDefined();
        expect(actual).toHaveLength(3);

        expect(actual[0]).toStrictEqual({
          commands: [0, 0, 0],
          newState: makeA8State(3, 0),
        });
        expect(actual[1]).toStrictEqual({
          commands: [1, 1, 0],
          newState: makeA8State(2, 2),
        });
        expect(actual[2]).toStrictEqual({
          commands: [1, 1],
          newState: makeA8State(0, 4),
        });
      },
  );

})
;
