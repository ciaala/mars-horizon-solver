import {electricityIndex} from '../solver/utility-solver.test';
import SolverWorkerClient from '../worker/SolverWorkerClient';
import ResourceMap, {generate} from './ResourceMap';
import SolverSolutionTransformer from './SolverSolutionTransformer';
import {setValueHolder} from '../reducer/MarsHorizonReducer';
const solverClient = new SolverWorkerClient();

/**
 * @typedef {object} SolverState
 * @property {{'-1': {output: {resourceId: string, emoji: string, counter: number}[], id: number},
 * '-2': {output: {resourceId: string, emoji: string, counter: number}[], id: number},
 * '-3': {output: {resourceId: string, emoji: string, counter: number}[], id: number}}} rules
 * @property {{'/root/solver': boolean,
 * '/root/turns': number,
 * '/root/commands': number,
 * '/root/requiredCommands': string,
 * '/root/solver/drift/minimum': number,
 * '/root/solver/drift/maximum': number,
 * '/root/solver/heat/minimum': number,
 * '/root/solver/heat/maximum': number,
 * '/root/solver/drift/isDriftReset': boolean
 * }} valueHolders
 */

/**
 *
 */
class MarsHorizonState {

  static #id = 0;

  static getNextId() {
    return MarsHorizonState.#id++;
  }

  static increment(state, ruleId, section, resourceId) {
    MarsHorizonState.update(state, ruleId, section, resourceId, 1);
  }

  static decrement(state, ruleId, section, resourceId) {
    MarsHorizonState.update(state, ruleId, section, resourceId, -1);
  }

  static createRule(state) {
 //   console.log('create rule', state);

    const id = MarsHorizonState.getNextId();
    state.rules[id] = {id: id, input: generate(0), output: generate(0)};
 //   console.log('STATE rules: ', state.rules);
  }

  static deleteRule(state, ruleId) {
    if (state.rules[ruleId]) {
      delete state.rules[ruleId];
    }
  }

  // TODO Private method
  static update(state, ruleId, section, resourceId, delta) {
    const rules = state.rules;

    const rule = rules[ruleId];

    const resources = section === 'input' ? rule.input : rule.output;
 //   console.log({resources, section});
    const resource = resources.find(
        resource => resource.resourceId === resourceId);
    const value = resource.counter;
 //   console.log('Update', {rules, ruleId, rule, resourceId, delta, resources, value});
    resource.counter = value + delta;
    if (ruleId >= 0 && resource.counter < 0) {
      resource.counter = 0;
    }
  }

  static runSolver(state, {dispatch}) {
    console.clear();
 //   console.log({dispatch});
    const payload = MarsHorizonState.getSolverContext(state);

    if (state.valueHolders['/root/requiredCommands']) {
      const requiredCommandsJSON = state.valueHolders['/root/requiredCommands'];
      if (requiredCommandsJSON.length > 0) {
        /**
         *
         * @type {number[]}
         */
        const requiredCommands = JSON.parse(requiredCommandsJSON);
        solverClient.run(payload, requiredCommands, (event) =>
            MarsHorizonState.updateWithBestSolution(event.data, payload.commands, payload.initialState, dispatch));
      }
    } else {
      solverClient.run(payload, [], (event) =>
          MarsHorizonState.updateWithBestSolution(event.data, payload.commands, payload.initialState, dispatch));
    }
    console.timeLog('solve');
    console.timeEnd('solve');

  }

  static getSolverContext(state) {


    const rules = state.rules;
    const table = Object.values(rules).map(rule => ({
      id: rule.id,
      input: rule.input,
      output: rule.output,
    })).filter(rule => rule.id >= 0);
    const serializedTable = JSON.parse(JSON.stringify(table));
  //  console.log('Table', serializedTable);
    console.time('solve');
  //  console.timeStamp('solve');
    const numericTable = MarsHorizonState.prepareNumericTable(serializedTable);
    const resourceKeys = Object.keys(ResourceMap);
    const initialState = MarsHorizonState.makeNumeric(
        JSON.parse(JSON.stringify(rules[-1].output)), resourceKeys);
    const desiredState = MarsHorizonState.makeNumeric(
        JSON.parse(JSON.stringify(rules[-2].output)), resourceKeys);
    const everyTurn = MarsHorizonState.makeNumeric(
        JSON.parse(JSON.stringify(rules[-3].output)), resourceKeys);

    const turns = MarsHorizonState.getValue(state, '/root/turns');
    const commands = MarsHorizonState.getValue(state, '/root/commands');
    const heat = {
      minimum: MarsHorizonState.getValue(state, '/root/solver/heat/minimum'),
      maximum: MarsHorizonState.getValue(state, '/root/solver/heat/maximum')
    };
    const drift = {
      minimum: MarsHorizonState.getValue(state, '/root/solver/drift/minimum'),
      maximum: MarsHorizonState.getValue(state, '/root/solver/drift/maximum'),
      isReset: MarsHorizonState.getValue(state, '/root/solver/drift/isReset')
    };
    const electricity = {
      minimum: MarsHorizonState.getValue(state,
          '/root/solver/electricity/minimum')
    };
    return {
      productionRules: numericTable,
      initialState,
      desiredState,
      everyTurn,
      turns,
      commands,
      config: {heat, drift, electricity}
    };
  }

  /**
   *
   * @param results
   * @param commands
   * @param initialState
   * @param dispatch
   */
  static updateWithBestSolution(results, commands, initialState, dispatch) {
  //  console.log({results});
    const solution = SolverSolutionTransformer.findBestSolution(results);
    /**
     *
     * @type {Array.<UITurnTask>}
     */
    const solutionTypeResolver = SolverSolutionTransformer.transform(solution, commands);
    const payload = {valueHolder: '/root/solution', value: solutionTypeResolver};
    dispatch(setValueHolder(payload));
  }

  /**
   * @param {SolverState} state
   * @param {string} valueHolder
   */
  static getValue(state, valueHolder) {
    const value = state.valueHolders[valueHolder];
    return JSON.parse(JSON.stringify(value));
  }

  static prepareNumericTable(table) {
    const resourceKeys = Object.keys(ResourceMap);
    const numericTable = table.map(rule => ({
          id: rule.id,
          input: MarsHorizonState.makeNumeric(rule.input, resourceKeys),
          output: MarsHorizonState.makeNumeric(rule.output, resourceKeys)
        }),
    );
    //console.log(numericTable);
    return numericTable;
  }

  /**
   *
   * @param {SolverState} state
   * @param {string} valueHolder
   * @returns {SolverState}
   */
  static updateValueHolder(state, valueHolder, delta) {
    state.valueHolders[valueHolder] += delta;
    return state;
  }

  /**
   *
   * @param {SolverState} state
   * @param {string} valueHolder
   * @returns {SolverState}
   */
  static toggleBooleanValueHolder(state, valueHolder) {
    const currentValue = state.valueHolders[valueHolder];
    state.valueHolders[valueHolder] = !currentValue;
    return state;
  }

  /**
   *
   * @param {SolverState} state
   * @param resourceState
   * @returns {SolverState}
   */
  static useAsNewState(state, resourceState) {
    state.rules['-1'] = {id: -1, output: resourceState};
    state.valueHolders['/root/solution'] = undefined;
    state.valueHolders['/root/turns']--;
    return state;
  }



  static makeNumeric(section, resourceKeys) {
    if (!section) {
      return undefined;
    }
    const resources = Object.fromEntries(
        section.map(rule => [rule.resourceId, rule.counter]));
    return resourceKeys.map(resourceId => resources[resourceId]);
  }

  /**
   *
   * @param {SolverState} state
   * @param {string} valueHolder
   * @param {any} value
   * @returns {SolverState}
   */
  static setValueHolder(state, valueHolder, value) {
  //  console.log('SetValueHolder', {state, valueHolder, value});
    state.valueHolders[valueHolder] = value;
    return state;
  }
}

export default MarsHorizonState;
