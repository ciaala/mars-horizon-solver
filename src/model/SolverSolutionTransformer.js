import {electricityIndex} from '../solver/utility-solver.test';

/**
 * @typedef {object} UITurnTask
 * @property {number[]} commands
 * @property {number[]} state
 */

class SolverSolutionTransformer {

  /**
   *
   * @param {SolverResult} solution
   * @param {number} numberOfCommands
   * @returns {Array.<UITurnTask>}
   */
  static transform(solution, numberOfCommands) {
    if (!solution) {
      return [];
    }
    const commandSequence = Object.values(solution.commands);
    const states = solution.states.reverse();
    console.log({solution, commandSequence, states});
    /**
     *
     * @type {Array.<UITurnTask>}
     */
    const transformation = commandSequence
        .filter(command => command !== -3)
        .reduce((turns, command, index) => {
              const v = Math.floor(index / numberOfCommands);
              let stateIndex = index + 2 + v;
              if (stateIndex >= states.length) {
                stateIndex = states.length - 1;
              }
              const arrayState = Array.from(states[stateIndex]);
              if (turns[v]) {
                turns[v].commands.push(command);
                turns[v].state = arrayState;
              //  console.log('state: ', v, stateIndex);
              } else {
                turns.push({commands: [command], state: arrayState});
              //  console.log('state: ', v, stateIndex);
              }
              return turns;
            },
            [],
        )
    ;
    console.log('transformation', transformation);
    return transformation;
  }
  static findBestSolution(solutions) {
    console.log(solutions.map(s=>[s.move, s.commands.toString(), s.states.toString()]));
    const {max, index} = solutions.reduce((acc, e, i) => {
      const finalState = e.states[0];
      if (finalState[electricityIndex] > acc.max) {
        acc.index = i;
        acc.max = finalState[electricityIndex];
      }
      return acc;
    }, {max: -1, index: -1});
    //console.log('best solutions: ', {max, index});
    if (index > 0) {
      return solutions[index];
    } else {
      return solutions[0];
    }
  }
}

export default SolverSolutionTransformer;
