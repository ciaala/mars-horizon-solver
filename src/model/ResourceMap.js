const ResourceMap = {
  'electricity': ':zap:',
  'communication': ':rainbow:',
  'navigation': ':compass:',
  'data': ':brick:',
  'astronaut': ':male-astronaut:',
  'fire': ':fire:',
  'rocket': ':rocket:',
  'drift': ':ice_skate:',
  'radiation': ':radioactive:',
};
export const ResourceIds = {
  FIRE: 'fire',
  DRIFT: 'drift',
  ELECTRICITY: 'electricity',
  DATA: 'data',
  ASTRONAUT: 'astronaut',
  ROCKET: 'rocket',
  RADIATION: 'radiation',
};

/**
 *
 * @param initialValue
 * @returns {{resourceId: string, emoji: string, counter: number}[]}
 */
export function generate(initialValue = 0) {
  return Object.entries(ResourceMap).map(
      ([key, value]) => ({
        resourceId: key,
        emoji: value,
        counter: initialValue,
      }),
  );
}

export function getResourceIndex(resourceId) {
  return Object.keys(ResourceMap).findIndex(r => r === resourceId);
}

export const NumberOfResources = Object.keys(ResourceMap).length;
export default ResourceMap;
