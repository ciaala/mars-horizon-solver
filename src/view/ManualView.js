import * as PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import Title from '../components/Title';
import Turn from '../components/Turn.js';
import React, {useState} from 'react';
import MarsHorizonState from '../model/MarsHorizonState';
import SolverSolutionTransformer from '../model/SolverSolutionTransformer';
import XxlSolver from '../solver/xxl-solver';
import styles from './ManualView.module.css';

/**
 *
 * @param minRulesId
 * @param maxRulesId
 * @param manualCommands
 * @param setManualCommands
 * @returns {function(*=, *=): void}
 */
function inputChangeCallbackFactory(
    minRulesId,
    maxRulesId,
    manualCommands,
    setManualCommands) {
  return uuid => target => onInputChange(target, uuid, minRulesId, maxRulesId, manualCommands, setManualCommands);
}

function onInputChange(value, uuid, minRulesId, maxRulesId, manualCommands, setManualCommands) {
  const ruleValue = parseInt(value, 10);
  if (value <= maxRulesId && value >= minRulesId) {
    const newValue = manualCommands.slice();
    newValue[uuid] = ruleValue;
    localStorage.setItem('manualCommands', JSON.stringify(newValue, undefined, 2));
    setManualCommands(newValue);
  }
  console.log('skipping', ruleValue);
  return ruleValue;
}

function uuidGeneratorFactory(commandsInEachTurn) {
  return (commandIndex, turnIndex) => (turnIndex * (commandsInEachTurn + 1)) + commandIndex;
}

/**
 * @param states
 */
function ManualInput(props) {
  return <input className={styles.ManualInput}
                value={props.value}
                onChange={e => props.onChange(e.target.value)}/>;
}

ManualInput.propTypes = {
  value: PropTypes.number,
  onChangeFactory: PropTypes.func,
  uuid: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number])
};

/**
 * @param commands
 */
function TurnInputList({turnCommands, turnIndex, uuidFactory, changeCallbackFactory}) {

  return <div className={styles.ManualInputList}>
    {turnCommands.map((c, commandIndex) => {
      const uuid = uuidFactory(commandIndex, turnIndex);
      return <ManualInput key={commandIndex}
                          uuid={uuid} value={c}
                          onChange={changeCallbackFactory(uuid)}/>;
    })
    }
  </div>;
}

/**
 * @param {number} minRulesId
 * @param {number} maxRulesId
 * @param {number} commands
 * @param {number} turns
 * @returns {number[]}
 */
function getManualCommands(minRulesId, maxRulesId, commands, turns) {
  const expectedElements = ((commands + 1) * turns) - 1;
  /**
   *
   * @type {Array.<number>}
   */
  const localManualCommands = JSON.parse(localStorage.getItem('manualCommands'));
  if (Array.isArray(localManualCommands) &&
      localManualCommands.every(e => (((e >= minRulesId) && (e <= maxRulesId)) || (e === -3)))) {
    return localManualCommands.splice(0, expectedElements);
  }

  /**
   *
   * @type {number[]}
   */
  const defaultCommands = Array.from({length: expectedElements}).fill(0);
  return defaultCommands.map(
      (e, i) => {
        if (((i + 1) % (commands + 1)) === 0) {
          return -3;
        } else {
          return minRulesId;
        }
      });
}

export default function ManualView() {
  const solverContext = useSelector(state => MarsHorizonState.getSolverContext(state));
  const allRules = useSelector(state => state.rules);

  const solver = new XxlSolver(
      solverContext.productionRules,
      solverContext.initialState,
      solverContext.desiredState,
      solverContext.everyTurn,
      solverContext.turns,
      solverContext.commands,
      solverContext.config
  );

  const arrayRules = Object.entries(allRules).filter(
      ([id, r]) => id >= 0).map(
          (e, i) => [i, e[1]]);

  const [minRulesId, maxRulesId] = arrayRules.reduce(
      ([min, max], [, {id}]) =>
          ([Math.min(id, min), Math.max(id, max)])
      , [Math.pow(2, 16), 0]);

  const rules = Object.fromEntries(arrayRules);

  const initCommands = getManualCommands(minRulesId, maxRulesId, solverContext.commands, solverContext.turns);
  const [manualCommands, setManualCommands] = useState(initCommands);

  const commandsInt8Array = new Int8Array(manualCommands);
  const initialStateInt8Array = new Int8Array(solverContext.initialState);

  const statesInt8Array = solver.createStateHistory(initialStateInt8Array, commandsInt8Array);
  /**
   *
   * @type {SolverResult}
   */
  const solution = {move: 0, commands: commandsInt8Array, states: statesInt8Array};
  console.log({solution});
  const manualTurns = SolverSolutionTransformer.transform(solution, solverContext.commands);

  const changeCallbackFactory = inputChangeCallbackFactory(minRulesId, maxRulesId, manualCommands, setManualCommands);
  const uuidFactory = uuidGeneratorFactory(solverContext.commands);
  //
  return <div className={styles.ManualView}>
    <Title text={'Manual Plan'}/>
    {manualTurns.map((e, index) =>
        <div key={index}>
          <TurnInputList turnCommands={e.commands}
                         turnIndex={index}
                         changeCallbackFactory={changeCallbackFactory}
                         uuidFactory={uuidFactory}
          />
          <Turn key={index} rules={rules} state={e.state} commands={e.commands}/>
        </div>)
    }
  </div>;

}
//
// <ActionBar>
//   <Action emojiIdentifier={':star:'} onClick={addClickHandler(manualCommands)}/>
// </ActionBar>
