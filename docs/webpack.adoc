= WebPack

Core Concepts of WebPack

* https://webpack.js.org/concepts/

== Configuration

* https://webpack.js.org/configuration/mode/[Mode]

== Extra

* https://webpack.js.org/guides/development-vagrant/[Vagrant]
** If you have a more advanced project and use Vagrant to run your development environment in a Virtual Machine, you'll often want to also run webpack in the VM.
* https://stackoverflow.com/questions/61755999/uncaught-referenceerror-regeneratorruntime-is-not-defined-in-react[Babel and WebPack]
* https://github.com/webpack/webpack/issues/9708[WebPack Babel - Class Properties]
* https://webpack.js.org/loaders/babel-loader/[WebPack Babel-Loader]
* https://webpack.js.org/guides/development/[WebPack]
* https://webpack.js.org/plugins/source-map-dev-tool-plugin/#examples[SourceMap]
